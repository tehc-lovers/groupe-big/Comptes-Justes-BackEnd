﻿using Infrastructure.SqlServer.Utils;

namespace Application.Validators
{
    public class ElementExistValidator<T>:AbstractValidator<int>
    {
        private readonly IElementQueryableById<T> _repo;

        public ElementExistValidator(IElementQueryableById<T> repo)
        {
            _repo = repo;
        }


        public override bool IsValid(int data)
        {
            var elementFound = _repo.GetById(data);
            if (elementFound != null) return true;
            AddErrorMessage($"Element does not exist with id {data}");
            return false;
        }
    }
}