﻿using System;
using Application.UseCases.Users.Dtos;
using Infrastructure.SqlServer.Repositories.User;

namespace Application.Validators
{
    public class ValidatorUsers
    {
        private readonly IUserRepository _userRepository;

        public ValidatorUsers(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public bool IsValid(int id)
        {
            return _userRepository.GetById(id) != null;
        }

        public bool IsDataNotNull(InputDtoUser user)
        {
            return !string.IsNullOrEmpty(user.FirstName) && !string.IsNullOrEmpty(user.LastName) &&
                   !string.IsNullOrEmpty(user.Email)&& !string.IsNullOrEmpty(user.Nickname)&&
                   !string.IsNullOrEmpty(user.Password)&& !string.IsNullOrEmpty(user.AccountNumber);
            /* return user.Email != null && user.FirstName != null &&
                    user.LastName != null && user.Password != null &&
                    user.Pseudo != null && user.CompteNumber != null;*/
        }

        public bool IsDataValid(InputDtoUser user)
        {
            return user.FirstName.Equals("") && user.LastName.Equals("") &&
                   user.Email.Equals("") && user.Password.Equals("") &&
                   user.Nickname.Equals("") && user.AccountNumber.Equals("");
        }
    }
}