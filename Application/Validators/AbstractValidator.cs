﻿using System.Collections.Generic;
using System.Linq;

namespace Application.Validators
{
    public abstract class AbstractValidator<TIn>
    {
        private readonly List<string> _errorMessages = new List<string>();
        public abstract bool IsValid(TIn data);

        protected void AddErrorMessage(string message)
        {
            _errorMessages.Add(message);
        }

        public string CompileErrorMessages(string glue = "")
        {
            return _errorMessages.Aggregate((acc, message) => acc + glue + message);
        }
    }
}