﻿using System;
using System.Collections.Generic;
using Application.UseCases.TransactionUser.Dtos;
using Application.UseCases.Utils;
using Application.Validators;
using Infrastructure.SqlServer.Repositories.Transaction;
using Infrastructure.SqlServer.Repositories.TransactionsUsers;
using Infrastructure.SqlServer.Repositories.User;

namespace Application.UseCases.TransactionUser
{
    public class UseCaseCreateTransactionUser:IWriting<OutputDtoTransactionUser, InputDtoTransactionUser>
    {
        private readonly IUserRepository _userRepository;
        private readonly ITransactionRepository _transactionRepository;
        private readonly ITransactionUserRepository _transactionUserRepository;

        public UseCaseCreateTransactionUser(ITransactionUserRepository transactionUserRepository, ITransactionRepository transactionRepository, IUserRepository userRepository)
        {
            _transactionUserRepository = transactionUserRepository;
            _transactionRepository = transactionRepository;
            _userRepository = userRepository;
        }

        public OutputDtoTransactionUser Execute(InputDtoTransactionUser dto)
        {
            ThrowIdDataIsValid(dto);
            var itemFromDto = Mapper.GetInstance().Map<Domain.TransactionUser>(dto);
            var itemFromDb = _transactionUserRepository.Create(itemFromDto);
            return Mapper.GetInstance().Map<OutputDtoTransactionUser>(itemFromDb);
        }

        private void ThrowIdDataIsValid(InputDtoTransactionUser inputDtoTransactionUser)
        {
            var userValidator = new ElementExistValidator<Domain.User>(_userRepository);
            if (!userValidator.IsValid(inputDtoTransactionUser.IdUser))
                throw new KeyNotFoundException(userValidator.CompileErrorMessages());

            var transactionValidator = new ElementExistValidator<Domain.Transaction>(_transactionRepository);
            if (!transactionValidator.IsValid(inputDtoTransactionUser.IdTransaction))
                throw new KeyNotFoundException(transactionValidator.CompileErrorMessages());
            if (_transactionUserRepository.GetAllByIds(inputDtoTransactionUser.IdUser,
                inputDtoTransactionUser.IdTransaction).Count !=0)
                throw new Exception("Element already exist!");
        }
    }
}