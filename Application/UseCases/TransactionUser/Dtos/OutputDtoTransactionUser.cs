﻿namespace Application.UseCases.TransactionUser.Dtos
{
    public class OutputDtoTransactionUser
    {
        public int Id { get; set; }
        public int IdUser { get; set; }
        public int IdTransaction { get; set; }
    }
}