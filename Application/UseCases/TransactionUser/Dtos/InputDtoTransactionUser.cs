﻿namespace Application.UseCases.TransactionUser.Dtos
{
    public class InputDtoTransactionUser
    {
        public int IdUser { get; set; }
        public int IdTransaction { get; set; }
    }
}