﻿using System.Collections.Generic;
using Application.UseCases.TransactionUser.Dtos;
using Application.UseCases.Utils;
using Infrastructure.SqlServer.Repositories.TransactionsUsers;

namespace Application.UseCases.TransactionUser
{
    public class UseCaseGetAllTransactionsUser:IQuery<List<OutputDtoTransactionUser>>
    {

        private readonly ITransactionUserRepository _transactionUserRepository;

        public UseCaseGetAllTransactionsUser(ITransactionUserRepository transactionUserRepository)
        {
            _transactionUserRepository = transactionUserRepository;
        }

        public List<OutputDtoTransactionUser> Execute()
        {
            List<Domain.TransactionUser> transactionUsers = _transactionUserRepository.GetAll();
            return Mapper.GetInstance().Map<List<OutputDtoTransactionUser>>(transactionUsers);
        }
    }
}