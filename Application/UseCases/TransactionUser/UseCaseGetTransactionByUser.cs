﻿using System.Collections.Generic;
using Application.UseCases.Transaction.Dtos;
using Application.UseCases.Utils;
using Application.Validators;
using Infrastructure.SqlServer.Repositories.TransactionsUsers;
using Infrastructure.SqlServer.Repositories.User;

namespace Application.UseCases.TransactionUser
{
    public class UseCaseGetTransactionByUser:IQueryFiltering<List<OutputDtoTransaction>, int>
    {
        private readonly IUserRepository _userRepository;
        private readonly ITransactionUserRepository _transactionUserRepository;

        public UseCaseGetTransactionByUser(ITransactionUserRepository transactionUserRepository, IUserRepository userRepository)
        {
            _transactionUserRepository = transactionUserRepository;
            _userRepository = userRepository;
        }

        public List<OutputDtoTransaction> Execute(int dto)
        {
            ThrowIdDataIsValid(dto);
            List<Domain.Transaction> transactions = _transactionUserRepository.GetAllTransactionByUser(dto);
            return Mapper.GetInstance().Map<List<OutputDtoTransaction>>(transactions);
        }

        public void ThrowIdDataIsValid(int dto)
        {
            var userValidator = new ElementExistValidator<Domain.User>(_userRepository);
            if (!userValidator.IsValid(dto))
                throw new KeyNotFoundException(userValidator.CompileErrorMessages());
        }
    }
}