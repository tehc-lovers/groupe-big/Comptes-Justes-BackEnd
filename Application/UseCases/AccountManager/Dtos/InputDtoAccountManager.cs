﻿namespace Application.UseCases.AccountManager.Dtos
{
    public class InputDtoAccountManager
    {
        public int IdAccount { get; set; }
        public int IdManager { get; set; }
    }
}