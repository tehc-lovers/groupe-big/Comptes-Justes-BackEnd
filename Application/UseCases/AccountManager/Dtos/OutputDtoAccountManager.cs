﻿namespace Application.UseCases.AccountManager.Dtos
{
    public class OutputDtoAccountManager
    {
        public int Id { get; set; }
        public int IdAccount { get; set; }
        public int IdManager { get; set; }
    }
}