﻿using System;
using System.Collections.Generic;
using System.Data;
using Application.UseCases.AccountManager.Dtos;
using Application.UseCases.Utils;
using Application.Validators;
using Infrastructure.SqlServer.Repositories.Account;
using Infrastructure.SqlServer.Repositories.AccountManager;
using Infrastructure.SqlServer.Repositories.User;

namespace Application.UseCases.AccountManager
{
    public class UseCaseCreateAccountManager:IWriting<OutputDtoAccountManager, InputDtoAccountManager>
    {
        private readonly IUserRepository _userRepository;
        private readonly IAccountRepository _accountRepository;
        private readonly IAccountManagerRepository _accountManagerRepository;

        public UseCaseCreateAccountManager(IUserRepository userRepository, IAccountRepository accountRepository, IAccountManagerRepository accountManagerRepository)
        {
            _userRepository = userRepository;
            _accountRepository = accountRepository;
            _accountManagerRepository = accountManagerRepository;
        }

        public OutputDtoAccountManager Execute(InputDtoAccountManager dto)
        {
            ThrowIdDataIsInvalid(dto);
            var itemFromDto = Mapper.GetInstance().Map<Domain.AccountManager>(dto);
            var itemFromDb = _accountManagerRepository.Create(itemFromDto);
            return Mapper.GetInstance().Map<OutputDtoAccountManager>(itemFromDb);
        }

        private void ThrowIdDataIsInvalid(InputDtoAccountManager dto)
        {
            var userValidator = new ElementExistValidator<Domain.User>(_userRepository);
            if (!userValidator.IsValid(dto.IdManager))
                throw new KeyNotFoundException(userValidator.CompileErrorMessages());

            var accountValidator = new ElementExistValidator<Domain.Account>(_accountRepository);
            if (!accountValidator.IsValid(dto.IdAccount))
                throw new KeyNotFoundException(accountValidator.CompileErrorMessages());

            if (_accountManagerRepository.GetAccountByAccountId(dto.IdAccount) != null)
                throw new Exception("Element already present!");

        }
    }
}