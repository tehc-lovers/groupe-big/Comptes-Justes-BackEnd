﻿using System.Collections.Generic;
using Application.UseCases.AccountManager.Dtos;
using Application.UseCases.Accounts;
using Application.UseCases.Accounts.Dtos;
using Application.UseCases.Utils;
using Application.Validators;
using Infrastructure.SqlServer.Repositories.AccountManager;
using Infrastructure.SqlServer.Repositories.User;

namespace Application.UseCases.AccountManager
{
    public class UseCaseGetAccountByManager:IQueryFiltering<List<OutputDtoAccount>, int>
    {
        private readonly IUserRepository _userRepository;
        private readonly IAccountManagerRepository _accountManagerRepository;

        public UseCaseGetAccountByManager(IAccountManagerRepository accountManagerRepository, IUserRepository userRepository)
        {
            _accountManagerRepository = accountManagerRepository;
            _userRepository = userRepository;
        }

        public List<OutputDtoAccount> Execute(int dto)
        {
            ThrowIdDataIsValid(dto);
            List<Domain.Account> accountManagers = _accountManagerRepository.GetByManagerId(dto);
            return Mapper.GetInstance().Map<List<OutputDtoAccount>>(accountManagers);
        }

        private void ThrowIdDataIsValid(int dto)
        {
            var userValidator = new ElementExistValidator<Domain.User>(_userRepository);
            if (!userValidator.IsValid(dto))
                throw new KeyNotFoundException(userValidator.CompileErrorMessages());
        }
    }
}