﻿using System;
using System.Collections.Generic;
using Application.UseCases.AccountUser.Dtos;
using Application.UseCases.Utils;
using Application.Validators;
using Infrastructure.SqlServer.Repositories.Account;
using Infrastructure.SqlServer.Repositories.AccountUser;
using Infrastructure.SqlServer.Repositories.User;

namespace Application.UseCases.AccountUser
{
    public class UseCaseCreateAccountUser:IWriting<OutputDtoAccountUser, InputDtoAccountUser>
    {
        private readonly IUserRepository _userRepository;
        private readonly IAccountRepository _accountRepository;
        private readonly IAccountUserRepository _accountUserRepository;

        public UseCaseCreateAccountUser(IAccountUserRepository accountUserRepository, IAccountRepository accountRepository, IUserRepository userRepository)
        {
            _accountUserRepository = accountUserRepository;
            _accountRepository = accountRepository;
            _userRepository = userRepository;
        }


        public OutputDtoAccountUser Execute(InputDtoAccountUser dto)
        {
            ThrowIdDataIsInvalid(dto);
            var itemFromDto = Mapper.GetInstance().Map<Domain.AccountUser>(dto);
            var itemFromDb = _accountUserRepository.Create(itemFromDto);
            return Mapper.GetInstance().Map<OutputDtoAccountUser>(itemFromDb);
        }
        
        private void ThrowIdDataIsInvalid(InputDtoAccountUser dto)
        {
            var userValidator = new ElementExistValidator<Domain.User>(_userRepository);
            if (!userValidator.IsValid(dto.IdUser))
                throw new KeyNotFoundException(userValidator.CompileErrorMessages());

            var accountValidator = new ElementExistValidator<Domain.Account>(_accountRepository);
            if (!accountValidator.IsValid(dto.IdAccount))
                throw new KeyNotFoundException(accountValidator.CompileErrorMessages());

            if (_accountUserRepository.GetAccountByAccountId(dto.IdAccount, dto.IdUser) != null)
                throw new Exception("Element already present!");

        }
    }
}