﻿using System.Collections.Generic;
using Application.UseCases.Utils;
using Application.Validators;
using Infrastructure.SqlServer.Repositories.Account;
using Infrastructure.SqlServer.Repositories.AccountUser;
using Infrastructure.SqlServer.Repositories.User;

namespace Application.UseCases.AccountUser
{
    public class UseCaseDeleteAccountUser:IUpdate<bool, int, int>
    {

        private readonly IUserRepository _userRepository;
        private readonly IAccountRepository _accountRepository;
        private readonly IAccountUserRepository _accountUserRepository;

        public UseCaseDeleteAccountUser(IAccountUserRepository accountUserRepository, IAccountRepository accountRepository, IUserRepository userRepository)
        {
            _accountUserRepository = accountUserRepository;
            _accountRepository = accountRepository;
            _userRepository = userRepository;
        }

        public bool Execute(int idUser, int idAccount)
        {
            ThrowIdDataIsValid(idUser, idAccount);
            return _accountUserRepository.DeleteByIdAccountAndIdUser(idAccount, idUser);
        }

        private void ThrowIdDataIsValid(int idUser, int idAccount)
        {
            var userValidator = new ElementExistValidator<Domain.User>(_userRepository);
            if (!userValidator.IsValid(idUser))
                throw new KeyNotFoundException(userValidator.CompileErrorMessages());

            var accountValidator = new ElementExistValidator<Domain.Account>(_accountRepository);
            if (!accountValidator.IsValid(idAccount))
                throw new KeyNotFoundException(accountValidator.CompileErrorMessages());
            
        }
    }
}