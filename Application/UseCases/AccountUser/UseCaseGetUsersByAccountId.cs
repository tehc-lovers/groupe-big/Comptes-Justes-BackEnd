﻿using System.Collections.Generic;
using Application.UseCases.AccountUser.Dtos;
using Application.UseCases.Users.Dtos;
using Application.UseCases.Utils;
using Application.Validators;
using Infrastructure.SqlServer.Repositories.Account;
using Infrastructure.SqlServer.Repositories.AccountUser;

namespace Application.UseCases.AccountUser
{
    public class UseCaseGetUsersByAccountId:IQueryFiltering<List<OutputDtoUser>, int>
    {
        private readonly IAccountRepository _accountRepository;
        private readonly IAccountUserRepository _accountUserRepository;

        public UseCaseGetUsersByAccountId(IAccountRepository accountRepository, IAccountUserRepository accountUserRepository)
        {
            _accountRepository = accountRepository;
            _accountUserRepository = accountUserRepository;
        }

        public List<OutputDtoUser> Execute(int dto)
        {
            ThrowIdDataIsValid(dto);
            List<Domain.User> users = _accountUserRepository.GetUsersByAccountId(dto);
            return Mapper.GetInstance().Map<List<OutputDtoUser>>(users);
        }

        public void ThrowIdDataIsValid(int dto)
        {
            var accountValidator = new ElementExistValidator<Domain.Account>(_accountRepository);
            if (!accountValidator.IsValid(dto))
                throw new KeyNotFoundException(accountValidator.CompileErrorMessages());
        }
    }
}