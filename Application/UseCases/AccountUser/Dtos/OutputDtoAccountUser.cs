﻿using System;

namespace Application.UseCases.AccountUser.Dtos
{
    public class OutputDtoAccountUser
    {
        public int Id { get; set; }
        public int IdAccount { get; set; }
        public int IdUser { get; set; }
        public DateTime AddedDate { get; set; }
    }
}