﻿namespace Application.UseCases.AccountUser.Dtos
{
    public class InputDtoAccountUser
    {
        public int IdAccount { get; set; }
        public int IdUser { get; set; }
    }
}