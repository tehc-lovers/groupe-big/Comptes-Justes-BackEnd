﻿using System.Collections.Generic;
using Application.UseCases.Accounts;
using Application.UseCases.Accounts.Dtos;
using Application.UseCases.Utils;
using Application.Validators;
using Infrastructure.SqlServer.Repositories.AccountManager;
using Infrastructure.SqlServer.Repositories.AccountUser;
using Infrastructure.SqlServer.Repositories.User;

namespace Application.UseCases.AccountUser
{
    public class UseCaseGetAccountsByUserId: IQueryFiltering<List<OutputDtoAccount>, int>
    {
        private readonly IUserRepository _userRepository;
        private readonly IAccountUserRepository _accountUserRepository;

        public UseCaseGetAccountsByUserId(IUserRepository userRepository, IAccountUserRepository accountUserRepository)
        {
            _userRepository = userRepository;
            _accountUserRepository = accountUserRepository;
        }


        public List<OutputDtoAccount> Execute(int dto)
        {
            ThrowIdDataIsValid(dto);
            List<Domain.Account> comptes = _accountUserRepository.GetAccountsByUserId(dto);
            return Mapper.GetInstance().Map<List<OutputDtoAccount>>(comptes);
        }

        public void ThrowIdDataIsValid(int dto)
        {
            var userValidator = new ElementExistValidator<Domain.User>(_userRepository);
            if (!userValidator.IsValid(dto))
                throw new KeyNotFoundException(userValidator.CompileErrorMessages());
            
        }
    }
}