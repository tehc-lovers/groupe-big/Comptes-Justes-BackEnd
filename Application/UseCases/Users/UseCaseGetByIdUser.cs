﻿using System.Collections.Generic;
using Application.UseCases.Users.Dtos;
using Application.UseCases.Utils;
using Application.Validators;
using Infrastructure.SqlServer.Repositories.User;

namespace Application.UseCases.Users
{
    public class UseCaseGetByIdUser:IQueryFiltering<OutputDtoUser, int>
    {
        private readonly IUserRepository _userRepository;
        private readonly ValidatorUsers _validatorUsers;

        public UseCaseGetByIdUser(IUserRepository userRepository, ValidatorUsers validatorUsers)
        {
            _userRepository = userRepository;
            _validatorUsers = validatorUsers;
        }

        public OutputDtoUser Execute(int dto)
        {
            if (!_validatorUsers.IsValid(dto))
                throw new KeyNotFoundException();
                
            Domain.User user = _userRepository.GetById(dto);
            return Mapper.GetInstance().Map<OutputDtoUser>(user);
        }
    }
}