﻿namespace Application.UseCases.Users.Dtos
{
    public class InputDtoUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Nickname { get; set; }
        public string Password { get; set; }
        public string AccountNumber { get; set; }
    }
}