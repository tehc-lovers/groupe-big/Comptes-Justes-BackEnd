﻿using System;
using Domain;

namespace Application.UseCases.Users.Dtos
{
    public class OutputDtoUser
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Nickname { get; set; }
        public string Password { get; set; }
        public string AccountNumber { get; set; }
        public void UpDate(InputDtoUser userModifier)
        {
            if(!string.IsNullOrEmpty(userModifier.FirstName))
                FirstName = userModifier.FirstName;
            if(!string.IsNullOrEmpty(userModifier.LastName))
                LastName = userModifier.LastName;
            if(!string.IsNullOrEmpty(userModifier.Email))
                Email = userModifier.Email;
            if(!string.IsNullOrEmpty(userModifier.Nickname))
                Nickname = userModifier.Nickname;
            if(!string.IsNullOrEmpty(userModifier.Password))
                Password = userModifier.Password;
            if(!string.IsNullOrEmpty(userModifier.AccountNumber))
                AccountNumber = userModifier.AccountNumber;
        }

        protected bool Equals(OutputDtoUser other)
        {
            return Id == other.Id && FirstName == other.FirstName 
                                  && LastName == other.LastName
                                  && Email == other.Email 
                                  && Nickname == other.Nickname
                                  && Password == other.Password 
                                  && AccountNumber == other.AccountNumber;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((OutputDtoUser) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, FirstName, LastName, Email, Nickname, Password, AccountNumber);
        }
    }
}