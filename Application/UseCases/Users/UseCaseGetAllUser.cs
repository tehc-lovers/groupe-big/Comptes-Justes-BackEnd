﻿using System.Collections.Generic;
using Application.UseCases.Users.Dtos;
using Application.UseCases.Utils;
using Domain;
using Infrastructure.SqlServer.Repositories.User;

namespace Application.UseCases.Users
{
    public class UseCaseGetAllUser:IQuery<List<OutputDtoUser>>
    {
        private IUserRepository _userRepository;

        public UseCaseGetAllUser(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public List<OutputDtoUser> Execute()
        {
            List<User> users = _userRepository.GetAll();
            return Mapper.GetInstance().Map<List<OutputDtoUser>>(users);
        }
    }
}