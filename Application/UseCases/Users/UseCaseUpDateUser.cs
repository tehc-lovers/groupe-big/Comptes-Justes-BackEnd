﻿using System;
using System.Collections.Generic;
using Application.UseCases.Users.Dtos;
using Application.UseCases.Utils;
using Application.Validators;
using Infrastructure.SqlServer.Repositories.User;

namespace Application.UseCases.Users
{
    public class UseCaseUpDateUser : IUpdate<OutputDtoUser, int, InputDtoUser>
    {
        private readonly IUserRepository _userRepository;
        private readonly ValidatorUsers _validatorUsers;

        public UseCaseUpDateUser(IUserRepository userRepository, ValidatorUsers validatorUsers)
        {
            _userRepository = userRepository;
            _validatorUsers = validatorUsers;
        }

        public OutputDtoUser Execute(int id, InputDtoUser modifiant)
        {
            if (!_validatorUsers.IsValid(id))
                throw new KeyNotFoundException();
            /*if (!_validatorUsers.IsDataNotNull(modifiant))
                throw new NullReferenceException();*/

            OutputDtoUser outUser = Mapper.GetInstance().Map<OutputDtoUser>(_userRepository.GetById(id));
            outUser.UpDate(modifiant);
            Domain.User userModifer = Mapper.GetInstance().Map<Domain.User>(outUser);
            
            _userRepository.Update(id, userModifer);
            return Mapper.GetInstance().Map<OutputDtoUser>(_userRepository.GetById(id));
            


        }
    }
}