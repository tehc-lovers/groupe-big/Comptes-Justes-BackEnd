using System;

namespace Application.UseCases.Transaction.Dtos
{
    public class InputDtoTransaction
    {
        public double Amount { get; set; }
        public string Date { get; set; }
        public int  Level { get; set; }
        public string Communication { get; set; }
    }
}