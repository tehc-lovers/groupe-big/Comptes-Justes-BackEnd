using Application.UseCases.Transaction.Dtos;
using Application.UseCases.Utils;
using Infrastructure.SqlServer.Repositories.Transaction;

namespace Application.UseCases.Transaction
{
    public class UseCaseGetByIdTransaction
    {
        private ITransactionRepository _transactionRepository;

        public UseCaseGetByIdTransaction(ITransactionRepository transactionRepository)
        {
            _transactionRepository = transactionRepository;
        }

        public OutputDtoTransaction Execute(int dto)
        {
            Domain.Transaction transaction = _transactionRepository.GetById(dto);
            return Mapper.GetInstance().Map<OutputDtoTransaction>(transaction);
        }
    }
}