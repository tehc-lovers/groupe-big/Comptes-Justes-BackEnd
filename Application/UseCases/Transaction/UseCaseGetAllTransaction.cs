using System.Collections.Generic;
using Application.UseCases.Transaction.Dtos;
using Application.UseCases.Utils;
using Infrastructure.SqlServer.Repositories.Transaction;

namespace Application.UseCases.Transaction
{
    public class UseCaseGetAllTransaction : IQuery<List<OutputDtoTransaction>>
    {
        private readonly ITransactionRepository _transactionRepository;

        public UseCaseGetAllTransaction(ITransactionRepository transactionRepository)
        {
            _transactionRepository = transactionRepository;
        }

        public List<OutputDtoTransaction> Execute()
        {
            List<Domain.Transaction> transactions = _transactionRepository.GetAll();
            return Mapper.GetInstance().Map<List<OutputDtoTransaction>>(transactions);
        }
    }
}