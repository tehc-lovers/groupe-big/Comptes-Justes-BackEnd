using Application.UseCases.Transaction.Dtos;
using Application.UseCases.Utils;
using Infrastructure.SqlServer.Repositories.Transaction;


namespace Application.UseCases.Transaction
{
    public class UseCaseCreateTransaction : IWriting<OutputDtoTransaction, InputDtoTransaction>
    {
        private readonly ITransactionRepository _transactionRepository;

        public UseCaseCreateTransaction(ITransactionRepository transactionRepository)
        {
            _transactionRepository = transactionRepository;
        }

        public OutputDtoTransaction Execute(InputDtoTransaction dto)
        {
            var transactionFromDto = Mapper.GetInstance().Map<Domain.Transaction>(dto);
            var transactionFromDb = _transactionRepository.Create(transactionFromDto);
            return Mapper.GetInstance().Map<OutputDtoTransaction>(transactionFromDb);
        }
    }
}