﻿using Application.UseCases.Bill.Dtos;
using Application.UseCases.Utils;
using Infrastructure.SqlServer.Repositories.Bill;

namespace Application.UseCases.Bill
{
    public class UseCaseGetBillByIdTransaction:IQueryFiltering<OutputDtoBill, int>
    {
        private IBillRepository _billRepository;

        public UseCaseGetBillByIdTransaction(IBillRepository billRepository)
        {
            _billRepository = billRepository;
        }

        public OutputDtoBill Execute(int dto)
        {
            var itemFromDto = _billRepository.GetBillByIdTransaction(dto);
            return Mapper.GetInstance().Map<OutputDtoBill>(itemFromDto);
        }
    }
}