﻿using System;
using System.Collections.Generic;
using Application.UseCases.Bill.Dtos;
using Application.UseCases.Utils;
using Application.Validators;
using Infrastructure.SqlServer.Repositories.Bill;
using Infrastructure.SqlServer.Repositories.Transaction;

namespace Application.UseCases.Bill
{
    public class UseCaseCreateBill:IWriting<OutputDtoBill, InputDtoBill>
    {
        private ITransactionRepository _transactionRepository;
        private IBillRepository _billRepository;

        public UseCaseCreateBill(IBillRepository billRepository, ITransactionRepository transactionRepository)
        {
            _billRepository = billRepository;
            _transactionRepository = transactionRepository;
        }

        public OutputDtoBill Execute(InputDtoBill dto)
        {
            ThrowIdDataIsValid(dto);
            var itemFromDto = Mapper.GetInstance().Map<Domain.Bill>(dto);
            var itemFromDb = _billRepository.Create(itemFromDto);
            return Mapper.GetInstance().Map<OutputDtoBill>(itemFromDb);
        }

        private void ThrowIdDataIsValid(InputDtoBill dto)
        {
            var transactionValidator = new ElementExistValidator<Domain.Transaction>(_transactionRepository);
            if (!transactionValidator.IsValid(dto.IdTransaction))
                throw new KeyNotFoundException(transactionValidator.CompileErrorMessages());
            if (_billRepository.GetBillsByPath(dto.RelativePath).Count !=0)
                throw new Exception("This path is already used!");
        }
    }
}