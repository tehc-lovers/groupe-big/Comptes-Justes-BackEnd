﻿namespace Application.UseCases.Bill.Dtos
{
    public class OutputDtoBill
    {
        public int Id { get; set; }
        public int IdTransaction { get; set; }
        public string RelativePath { get; set; }
    }
}