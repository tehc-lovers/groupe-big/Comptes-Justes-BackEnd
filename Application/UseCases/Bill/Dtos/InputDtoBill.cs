﻿namespace Application.UseCases.Bill.Dtos
{
    public class InputDtoBill
    {
        public int IdTransaction { get; set; }
        public string RelativePath { get; set; }
    }
}