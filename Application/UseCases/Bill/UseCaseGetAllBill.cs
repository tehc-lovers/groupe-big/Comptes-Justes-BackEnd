﻿using System.Collections.Generic;
using Application.UseCases.Bill.Dtos;
using Application.UseCases.Utils;
using Infrastructure.SqlServer.Repositories.Bill;

namespace Application.UseCases.Bill
{
    public class UseCaseGetAllBill:IQuery<List<OutputDtoBill>>
    {
        private IBillRepository _billRepository;

        public UseCaseGetAllBill(IBillRepository billRepository)
        {
            _billRepository = billRepository;
        }

        public List<OutputDtoBill> Execute()
        {
            List<Domain.Bill> bills = _billRepository.GetAll();
            return Mapper.GetInstance().Map<List<OutputDtoBill>>(bills);
        }
    }
}