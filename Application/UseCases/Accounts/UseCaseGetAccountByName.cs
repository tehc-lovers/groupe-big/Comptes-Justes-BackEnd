using System.Collections.Generic;
using Application.UseCases.Accounts.Dtos;
using Application.UseCases.Utils;
using Infrastructure.SqlServer.Repositories.Account;

namespace Application.UseCases.Accounts
{
    public class UseCaseGetAccountByName:IQueryFiltering<List<OutputDtoAccount>, string>
    {
        private readonly IAccountRepository _accountRepository;

        public UseCaseGetAccountByName(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        public List<OutputDtoAccount> Execute(string dto)
        {
            List<Domain.Account> accounts = _accountRepository.GetByName(dto);
            return Mapper.GetInstance().Map<List<OutputDtoAccount>>(accounts);
        }
    }
}