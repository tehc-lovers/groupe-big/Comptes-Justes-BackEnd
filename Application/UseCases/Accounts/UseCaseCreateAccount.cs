using Application.UseCases.Accounts.Dtos;
using Application.UseCases.Utils;
using Infrastructure.SqlServer.Repositories.Account;

namespace Application.UseCases.Accounts
{
    public class UseCaseCreateAccount:IWriting<OutputDtoAccount, InputDtoAccount>
    {
        private readonly IAccountRepository _accountRepository;

        public UseCaseCreateAccount(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        public OutputDtoAccount Execute(InputDtoAccount dto)
        {
            var accountFromDto = Mapper.GetInstance().Map<Domain.Account>(dto);
            var accountFromDb = _accountRepository.Create(accountFromDto);
            return Mapper.GetInstance().Map<OutputDtoAccount>(accountFromDb);
        }
    }
}