namespace Application.UseCases.Accounts.Dtos
{
    public class OutputDtoAccount
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}