using System.Collections.Generic;
using Application.UseCases.Accounts.Dtos;
using Application.UseCases.Utils;
using Infrastructure.SqlServer.Repositories.Account;

namespace Application.UseCases.Accounts
{
    public class UseCaseGetAllAccounts:IQuery<List<OutputDtoAccount>>
    {
        private readonly IAccountRepository _accountRepository;

        public UseCaseGetAllAccounts(IAccountRepository accountRepository)
        {
            _accountRepository = accountRepository;
        }

        public List<OutputDtoAccount> Execute()
        {
            List<Domain.Account> accounts = _accountRepository.GetAll();
            return Mapper.GetInstance().Map<List<OutputDtoAccount>>(accounts);
        }
    }
}