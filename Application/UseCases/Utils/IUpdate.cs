﻿namespace Application.UseCases.Utils
{
    public interface IUpdate<out TO ,in TI, in TI2>
    {
        TO Execute(TI aModifier, TI2 modifiant);
    }
}