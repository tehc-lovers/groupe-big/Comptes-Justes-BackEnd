﻿using System;
using Application.UseCases.AccountManager.Dtos;
using Application.UseCases.Accounts;
using Application.UseCases.Accounts.Dtos;
using Application.UseCases.AccountUser.Dtos;
using Application.UseCases.Bill.Dtos;
using Application.UseCases.Transaction.Dtos;
using Application.UseCases.TransactionAccount.Dtos;
using Application.UseCases.TransactionUser.Dtos;
using Application.UseCases.Users.Dtos;
using AutoMapper;

namespace Application.UseCases.Utils
{
    public class Mapper
    {
        private static AutoMapper.Mapper _instance;

        public static AutoMapper.Mapper GetInstance()
        {
            return _instance ??= CreateMapper();
        }

        private static AutoMapper.Mapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                //source, destination
                //For Users
                cfg.CreateMap<InputDtoUser, Domain.User>();
                cfg.CreateMap<Domain.User, OutputDtoUser>();
                cfg.CreateMap<OutputDtoUser, Domain.User>();
                
                //For Transaction
                cfg.CreateMap<InputDtoTransaction, Domain.Transaction>();
                cfg.CreateMap<Domain.Transaction, OutputDtoTransaction>();
                
                //For Comptes
                cfg.CreateMap<InputDtoAccount, Domain.Account>();
                cfg.CreateMap<Domain.Account, OutputDtoAccount>();
                cfg.CreateMap<OutputDtoAccount, Domain.Account>();
                
                //For AccountManager
                cfg.CreateMap<InputDtoAccountManager, Domain.AccountManager>();
                cfg.CreateMap<Domain.AccountManager, OutputDtoAccountManager>();
                cfg.CreateMap<OutputDtoAccountManager, Domain.AccountManager>();
                
                //For AccountUser
                cfg.CreateMap<InputDtoAccountUser, Domain.AccountUser>();
                cfg.CreateMap<Domain.AccountUser,OutputDtoAccountUser >();
                cfg.CreateMap<OutputDtoAccountUser, Domain.AccountUser>();
                
                //For TransactionUser
                cfg.CreateMap<InputDtoTransactionUser, Domain.TransactionUser>();
                cfg.CreateMap<Domain.TransactionUser,OutputDtoTransactionUser>();
                cfg.CreateMap<OutputDtoTransactionUser, Domain.TransactionUser>();
                
                //For TransactionAccount
                cfg.CreateMap<InputDtoTransactionAccount, Domain.TransactionAccount>();
                cfg.CreateMap<OutputDtoTransactionAccount, Domain.TransactionAccount>();
                cfg.CreateMap<Domain.TransactionAccount, OutputDtoTransactionAccount>();
                
                //For Bill
                cfg.CreateMap<InputDtoBill, Domain.Bill>();
                cfg.CreateMap<OutputDtoBill, Domain.Bill>();
                cfg.CreateMap<Domain.Bill, OutputDtoBill>();
            });

            return new AutoMapper.Mapper(config);
        }
        
    }

    
}