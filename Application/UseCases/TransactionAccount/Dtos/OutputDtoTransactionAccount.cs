﻿namespace Application.UseCases.TransactionAccount.Dtos
{
    public class OutputDtoTransactionAccount
    {
        public int Id { get; set; }
        public int IdAccount { get; set; }
        public int IdTransaction { get; set; }
    }
}