﻿namespace Application.UseCases.TransactionAccount.Dtos
{
    public class InputDtoTransactionAccount
    {
        public int IdAccount { get; set; }
        public int IdTransaction { get; set; }
    }
}