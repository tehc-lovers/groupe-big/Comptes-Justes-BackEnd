﻿using System.Collections.Generic;
using Application.UseCases.Transaction.Dtos;
using Application.UseCases.Utils;
using Application.Validators;
using Infrastructure.SqlServer.Repositories.Account;
using Infrastructure.SqlServer.Repositories.Transaction;
using Infrastructure.SqlServer.Repositories.TransactionAccount;

namespace Application.UseCases.TransactionAccount
{
    public class UseCaseGetTransactionsByAccount:IQueryFiltering<List<OutputDtoTransaction>, int>
    {
        private IAccountRepository _accountRepository;
        private ITransactionAccountRepository _transactionAccountRepository;

        public UseCaseGetTransactionsByAccount(ITransactionAccountRepository transactionAccountRepository, IAccountRepository accountRepository)
        {
            _transactionAccountRepository = transactionAccountRepository;
            _accountRepository = accountRepository;
        }

        public List<OutputDtoTransaction> Execute(int dto)
        {
            ThrowIdDataIsValid(dto);
            List<Domain.Transaction> transactions = _transactionAccountRepository.GetTransactionsByAccount(dto);
            return Mapper.GetInstance().Map<List<OutputDtoTransaction>>(transactions);
        }

        private void ThrowIdDataIsValid(int dto)
        {
            var accountValidator = new ElementExistValidator<Domain.Account>(_accountRepository);
            if (!accountValidator.IsValid(dto))
                throw new KeyNotFoundException(accountValidator.CompileErrorMessages());
        }
    }
}