﻿using System;
using System.Collections.Generic;
using Application.UseCases.TransactionAccount.Dtos;
using Application.UseCases.Utils;
using Application.Validators;
using Infrastructure.SqlServer.Repositories.Account;
using Infrastructure.SqlServer.Repositories.Transaction;
using Infrastructure.SqlServer.Repositories.TransactionAccount;

namespace Application.UseCases.TransactionAccount
{
    public class UseCaseCreateTransactionAccount:IWriting<OutputDtoTransactionAccount, InputDtoTransactionAccount>
    {
        private IAccountRepository _accountRepository;
        private ITransactionRepository _transactionRepository;
        private ITransactionAccountRepository _transactionAccountRepository;

        public UseCaseCreateTransactionAccount(ITransactionAccountRepository transactionAccountRepository, ITransactionRepository transactionRepository, IAccountRepository accountRepository)
        {
            _transactionAccountRepository = transactionAccountRepository;
            _transactionRepository = transactionRepository;
            _accountRepository = accountRepository;
        }

        public OutputDtoTransactionAccount Execute(InputDtoTransactionAccount dto)
        {
            ThrowIdDataIsValid(dto);
            var itemFromDto = Mapper.GetInstance().Map<Domain.TransactionAccount>(dto);
            var itemFromDb = _transactionAccountRepository.Create(itemFromDto);
            return Mapper.GetInstance().Map<OutputDtoTransactionAccount>(itemFromDb);
        }

        private void ThrowIdDataIsValid(InputDtoTransactionAccount dto)
        {
            var accountValidator = new ElementExistValidator<Domain.Account>(_accountRepository);
            if (!accountValidator.IsValid(dto.IdAccount))
                throw new KeyNotFoundException(accountValidator.CompileErrorMessages());
            
            var transactionValidator = new ElementExistValidator<Domain.Transaction>(_transactionRepository);
            if (!transactionValidator.IsValid(dto.IdTransaction))
                throw new KeyNotFoundException(transactionValidator.CompileErrorMessages());
            if (_transactionAccountRepository.GetAllByIds(dto.IdAccount, dto.IdTransaction).Count != 0)
                throw new Exception("Element already exist!");
        }
    }
}