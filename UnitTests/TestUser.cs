﻿using System;
using Application.UseCases.Users.Dtos;
using Domain;
using NUnit.Framework;

namespace UnitTests
{
    public class TestUser
    {
        [Test]
        public void TestUpdateUser_filledWithCorrectData_returnExpected()
        {
            var inputDtoUser = new InputDtoUser()
            {
                FirstName = "Guillaume",
                LastName = "Lambert",
                Email = "test",
                Password = "test",
                Nickname = "test",
                AccountNumber = "test"
            };
            var actual = CreateUser(1, "test",
                "test", "test", "test", "test", "test");

            var expected = CreateUser(1, "Lambert", "Guillaume", "test", "test",
                "test", "test");
            
            actual.UpDate(inputDtoUser);
            
            Assert.AreEqual(expected, actual);
        }


        [Test]
        public void TestUpdateUser_filledWithIncorrectData_returnNotExpected()
        {
            var modifiant=CreateInputUser("", "Guillaume", "test", "test",
                "test", "test");
            var actual= CreateUser(1, "test",
                "test", "test", "test", "test", "test");
            
            actual.UpDate(modifiant);
            
            var expeted=CreateUser(1, "test", "Guillaume", "test", "test",
                "test", "test");
            Assert.AreEqual(expeted,actual);
        }
        
        public InputDtoUser CreateInputUser(
            string name, string firstName, string email,
            string pseudo, string password, string compte)
        {
            return new InputDtoUser()
            {
               
                Email = email,
                Password = password,
                Nickname = pseudo,
                AccountNumber = compte,
                FirstName = firstName,
                LastName = name
            };
        }
        public OutputDtoUser CreateUser(int id,
            string name, string firstName, string email,
            string pseudo, string password, string compte)
        {
            return new OutputDtoUser()
            {
                Id = id,
                Email = email,
                Password = password,
                Nickname = pseudo,
                AccountNumber = compte,
                FirstName = firstName,
                LastName = name
            };
        }
        
    }
}