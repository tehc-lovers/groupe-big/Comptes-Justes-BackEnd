﻿namespace Domain
{
    public class AccountManager
    {
        public int Id { get; set; }
        public int IdAccount { get; set; }
        public int IdManager { get; set; }

        public AccountManager()
        {
        }

        public AccountManager(int id, int idAccount, int idManager)
        {
            Id = id;
            IdAccount = idAccount;
            IdManager = idManager;
        }
    }
}