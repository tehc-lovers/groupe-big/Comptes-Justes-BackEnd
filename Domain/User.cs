﻿using System;
using System.Collections.Generic;

namespace Domain
{
    public class User
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Nickname { get; set; }
        public string Password { get; set; }
        public string AccountNumber { get; set; }

        protected bool Equals(User other)
        {
            return Id == other.Id && FirstName == other.FirstName 
                                  && LastName == other.LastName
                                  && Email == other.Email 
                                  && Nickname == other.Nickname
                                  && Password == other.Password 
                                  && AccountNumber == other.AccountNumber;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((User) obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, FirstName, LastName, Email, Nickname, Password, AccountNumber);
        }
    }
}