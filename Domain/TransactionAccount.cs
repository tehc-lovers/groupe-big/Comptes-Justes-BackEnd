﻿namespace Domain
{
    public class TransactionAccount
    {
        public int Id { get; set; }
        public int IdAccount { get; set; }
        public int IdTransaction { get; set; }
    }
}