﻿namespace Domain
{
    public class Transaction
    {
        public int Id { get; set; }
        public decimal Amount { get; set; }
        public string Date { get; set; }
        public int  Level { get; set; }
        public string Communication { get; set; }
    }
}