﻿using System;

namespace Domain
{
    public class AccountUser
    {
        public int Id { get; set; }
        public int IdUser { get; set; } 
        public int IdAccount { get; set; }
        public DateTime AddedDate { get; set; }
    }
}