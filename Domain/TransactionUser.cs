﻿namespace Domain
{
    public class TransactionUser
    {
        public int Id { get; set; }
        public int IdUser { get; set; }
        public int IdTransaction { get; set; }
    }
}