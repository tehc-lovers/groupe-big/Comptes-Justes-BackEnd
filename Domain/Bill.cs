﻿namespace Domain
{
    public class Bill
    {
        public int Id { get; set; }
        public int IdTransaction { get; set; }
        public string RelativePath { get; set; }
    }
}