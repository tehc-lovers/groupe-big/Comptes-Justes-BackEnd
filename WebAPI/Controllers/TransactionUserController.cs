﻿using System;
using System.Collections.Generic;
using Application.UseCases.Transaction.Dtos;
using Application.UseCases.TransactionUser;
using Application.UseCases.TransactionUser.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/transactionUser")]
    public class TransactionUserController:ControllerBase
    {
        //UseCases
        private readonly UseCaseCreateTransactionUser _useCaseCreateTransactionUser;
        private readonly UseCaseGetTransactionByUser _useCaseGetTransactionByUser;
        private readonly UseCaseGetAllTransactionsUser _useCaseGetAllTransactionsUser;

        public TransactionUserController(UseCaseCreateTransactionUser useCaseCreateTransactionUser, UseCaseGetTransactionByUser useCaseGetTransactionByUser, UseCaseGetAllTransactionsUser useCaseGetAllTransactionsUser)
        {
            _useCaseCreateTransactionUser = useCaseCreateTransactionUser;
            _useCaseGetTransactionByUser = useCaseGetTransactionByUser;
            _useCaseGetAllTransactionsUser = useCaseGetAllTransactionsUser;
        }

        [HttpPost]
        [ProducesResponseType(201)]
        public ActionResult<OutputDtoTransactionUser> Create(InputDtoTransactionUser dto)
        {
            try
            {
                return StatusCode(201, _useCaseCreateTransactionUser.Execute(dto));
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
            
        }

        [HttpGet]
        [ProducesResponseType(201)]
        [Route("{idUser:int}")]
        public ActionResult<List<OutputDtoTransaction>> GetTransactionsByUser(int idUser)
        {
            return StatusCode(201, _useCaseGetTransactionByUser.Execute(idUser));
        }

        [HttpGet]
        [ProducesResponseType(201)]
        public ActionResult<List<OutputDtoTransactionUser>> GetAllTransactionUser()
        {
            return StatusCode(201, _useCaseGetAllTransactionsUser.Execute());
        }
    }
}