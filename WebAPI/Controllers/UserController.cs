﻿using System;
using System.Collections.Generic;
using Application.UseCases.Users;
using Application.UseCases.Users.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/users")]
    public class UserController:ControllerBase
    {
        private readonly UseCaseCreateUser _useCaseCreateUser;
        private readonly UseCaseGetByIdUser _useCaseGetByIdUser;
        private readonly UseCaseGetAllUser _useCaseGetAllUser;
        private readonly UseCaseUpDateUser _useCaseUpDateUser;

        
        public UserController(UseCaseCreateUser useCaseCreateUser,
            UseCaseGetAllUser useCaseGetAllUser, UseCaseGetByIdUser useCaseGetByIdUser, UseCaseUpDateUser useCaseUpDateUser)
        {
            _useCaseCreateUser = useCaseCreateUser;
            _useCaseGetAllUser = useCaseGetAllUser;
            _useCaseGetByIdUser = useCaseGetByIdUser;
            _useCaseUpDateUser = useCaseUpDateUser;
        }

        [HttpPost]
        [ProducesResponseType(201)]
        public ActionResult<OutputDtoUser> Create(InputDtoUser dto)
        {
            return StatusCode(201, _useCaseCreateUser.Execute(dto));
        }

        [HttpGet]
        [ProducesResponseType(201)]
        public ActionResult<List<Domain.User>> GetAll()
        {
            return StatusCode(201, _useCaseGetAllUser.Execute());
        }

        [HttpGet]
        [Route("{id:int}")]
        [ProducesResponseType(201)]
        [ProducesResponseType(404)]
        public ActionResult<OutputDtoUser> GetById(int id)
        {
            try
            {
                return _useCaseGetByIdUser.Execute(id);
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
            
        }

        [HttpPut]
        [Route("{id:int}")]
        [ProducesResponseType(201)]
        public ActionResult<OutputDtoUser> Update(int id,[FromBody] InputDtoUser modifiant)
        {
            try
            {
                return _useCaseUpDateUser.Execute(id, modifiant);
            }
            catch (KeyNotFoundException e)
            {
                return NotFound(e.Message);
            }
            catch (NullReferenceException e)
            {
                return Problem(e.Message);
            }
        }
    }
}