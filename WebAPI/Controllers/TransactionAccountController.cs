﻿using System;
using System.Collections.Generic;
using Application.UseCases.Transaction.Dtos;
using Application.UseCases.TransactionAccount;
using Application.UseCases.TransactionAccount.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/transactionAccount")]
    public class TransactionAccountController:ControllerBase
    {
        private UseCaseCreateTransactionAccount _useCaseCreateTransactionAccount;
        private UseCaseGetTransactionsByAccount _useCaseGetTransactionsByAccount;

        public TransactionAccountController(UseCaseCreateTransactionAccount useCaseCreateTransactionAccount, UseCaseGetTransactionsByAccount useCaseGetTransactionsByAccount)
        {
            _useCaseCreateTransactionAccount = useCaseCreateTransactionAccount;
            _useCaseGetTransactionsByAccount = useCaseGetTransactionsByAccount;
        }

        [HttpPost]
        [ProducesResponseType(201)]
        public ActionResult<OutputDtoTransactionAccount> Create(InputDtoTransactionAccount dto)
        {
            try
            {
                return StatusCode(201, _useCaseCreateTransactionAccount.Execute(dto));
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
            
        }

        [HttpGet]
        [ProducesResponseType(201)]
        [Route("{idAccount:int}")]
        public ActionResult<List<OutputDtoTransaction>> GetTransactionsByAccount(int idAccount)
        {
            return StatusCode(201, _useCaseGetTransactionsByAccount.Execute(idAccount));
        }
    }
}