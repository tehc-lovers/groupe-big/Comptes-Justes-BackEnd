﻿using System;
using System.Collections.Generic;
using Application.UseCases.AccountManager;
using Application.UseCases.AccountManager.Dtos;
using Application.UseCases.Accounts;
using Application.UseCases.Accounts.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/accountmanager")]
    public class AccountManagerController:ControllerBase
    {
        //UseCases
        private readonly UseCaseCreateAccountManager _useCaseCreateAccountManager;
        private readonly UseCaseGetAccountByManager _useCaseGetAccountByManager;

        public AccountManagerController(UseCaseCreateAccountManager useCaseCreateAccountManager, UseCaseGetAccountByManager useCaseGetAccountByManager)
        {
            _useCaseCreateAccountManager = useCaseCreateAccountManager;
            _useCaseGetAccountByManager = useCaseGetAccountByManager;
        }

        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(222)]
        public ActionResult<OutputDtoAccountManager> Create(InputDtoAccountManager dto)
        {
            try
            {
                return StatusCode(201, _useCaseCreateAccountManager.Execute(dto));
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }

        }

        [HttpGet]
        [ProducesResponseType(201)]
        [Route("{id:int}")]
        public ActionResult<List<OutputDtoAccount>> GetCompteByIdManager(int id)
        {
            return StatusCode(201, _useCaseGetAccountByManager.Execute(id));
        }
    }
}