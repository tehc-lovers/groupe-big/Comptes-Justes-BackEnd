﻿using System;
using System.Collections.Generic;
using Application.UseCases.Bill;
using Application.UseCases.Bill.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/bills")]
    public class BillController:ControllerBase
    {
        private readonly UseCaseCreateBill _useCaseCreateBill;
        private readonly UseCaseGetAllBill _useCaseGetAllBill;
        private readonly UseCaseGetBillByIdTransaction _useCaseGetBillByIdTransaction;

        public BillController(UseCaseCreateBill useCaseCreateBill, UseCaseGetAllBill useCaseGetAllBill, UseCaseGetBillByIdTransaction useCaseGetBillByIdTransaction)
        {
            _useCaseCreateBill = useCaseCreateBill;
            _useCaseGetAllBill = useCaseGetAllBill;
            _useCaseGetBillByIdTransaction = useCaseGetBillByIdTransaction;
        }

        [HttpPost]
        [ProducesResponseType(201)]
        public ActionResult<OutputDtoBill> Create(InputDtoBill dto)
        {
            try
            {
                return StatusCode(201, _useCaseCreateBill.Execute(dto));
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
            
        }

        [HttpGet]
        [ProducesResponseType(201)]
        public ActionResult<List<OutputDtoBill>> GetAll()
        {
            return StatusCode(201, _useCaseGetAllBill.Execute());
        }

        [HttpGet]
        [Route("{idTransaction:int}")]
        public ActionResult<OutputDtoBill> GetBillByIdTransaction(int idTransaction)
        {
            return StatusCode(200,_useCaseGetBillByIdTransaction.Execute(idTransaction));
        }
    }
}