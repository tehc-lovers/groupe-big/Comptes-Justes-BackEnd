﻿using System.Collections.Generic;
using Application.UseCases.Accounts;
using Application.UseCases.Accounts.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/accounts")]
    public class AccountController:ControllerBase
    {
        //UseCases
        private readonly UseCaseGetAllAccounts _useCaseGetAllAccounts;
        private readonly UseCaseCreateAccount _useCaseCreateAccount;
        private readonly UseCaseGetAccountByName _useCaseGetAccountByName;

        public AccountController(UseCaseGetAllAccounts useCaseGetAllAccounts,
            UseCaseCreateAccount useCaseCreateAccount, UseCaseGetAccountByName useCaseGetAccountByName)
        {
            _useCaseGetAllAccounts = useCaseGetAllAccounts;
            _useCaseCreateAccount = useCaseCreateAccount;
            _useCaseGetAccountByName = useCaseGetAccountByName;
        }

        [HttpGet]
        [ProducesResponseType(201)]
        public ActionResult<List<Domain.Account>> GetAll()
        {
            return StatusCode(201, _useCaseGetAllAccounts.Execute());
        }

        [HttpPost]
        [ProducesResponseType(201)]
        public ActionResult<OutputDtoAccount> Create(InputDtoAccount dto)
        {
            return StatusCode(201, _useCaseCreateAccount.Execute(dto));
        }

        [HttpGet]
        [Route("{name}")]
        [ProducesResponseType(201)]
        public ActionResult<List<Domain.Account>> GetByName(string name)
        {
            return StatusCode(201, _useCaseGetAccountByName.Execute(name));
        }
    }
}