using System.Collections.Generic;
using Application.UseCases.Transaction;
using Application.UseCases.Transaction.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/transactions")]
    
    public class TransactionController : ControllerBase
    {
        private readonly UseCaseCreateTransaction _useCaseCreateTransaction;
        private readonly UseCaseGetByIdTransaction _useCaseGetByIdTransaction;
        private readonly UseCaseGetAllTransaction _useCaseGetAllTransaction;

        public TransactionController(UseCaseCreateTransaction useCaseCreateTransaction,
            UseCaseGetAllTransaction useCaseGetAllTransaction, UseCaseGetByIdTransaction useCaseGetByIdTransaction)
        {
            _useCaseCreateTransaction = useCaseCreateTransaction;
            _useCaseGetAllTransaction = useCaseGetAllTransaction;
            _useCaseGetByIdTransaction = useCaseGetByIdTransaction;
        }

     /**   [HttpGet]
        [Route("{com:string}")]
        public List<OutputDtoTransaction> GetTransactionByCommunication(string com)
        {
            return _useCaseGetTransactionByCommunication.Execute(com);
        }*/

        [HttpPost]
        [ProducesResponseType(201)]
        public ActionResult<OutputDtoTransaction> Create(InputDtoTransaction dto)
        {
            return StatusCode(201, _useCaseCreateTransaction.Execute(dto));
        }

        [HttpGet]
        [ProducesResponseType(201)]
        public ActionResult<List<Domain.Transaction>> GetAll()
        {
            return StatusCode(201, _useCaseGetAllTransaction.Execute());
        }

        [HttpGet]
        [Route("{id:int}")]
        [ProducesResponseType(201)]
        public ActionResult<OutputDtoTransaction> GetById(int id)
        {
            return StatusCode(201, _useCaseGetByIdTransaction.Execute(id));
        }
    }
}

