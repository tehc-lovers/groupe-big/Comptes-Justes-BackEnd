﻿using System;
using System.Collections.Generic;
using Application.UseCases.Accounts;
using Application.UseCases.Accounts.Dtos;
using Application.UseCases.AccountUser;
using Application.UseCases.AccountUser.Dtos;
using Application.UseCases.Users.Dtos;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/accountuser")]
    public class AccountUserController:ControllerBase
    {
        //UseCases
        private readonly UseCaseCreateAccountUser _useCaseCreateAccountUser;
        private readonly UseCaseGetUsersByAccountId _useCaseGetUsersByAccountId;
        private readonly UseCaseGetAccountsByUserId _useCaseGetAccountsByUserId;
        private readonly UseCaseDeleteAccountUser _useCaseDeleteAccountUser;

        public AccountUserController(UseCaseCreateAccountUser useCaseCreateAccountUser, UseCaseGetUsersByAccountId useCaseGetUsersByAccountId, UseCaseGetAccountsByUserId useCaseGetAccountsByUserId, UseCaseDeleteAccountUser useCaseDeleteAccountUser)
        {
            _useCaseCreateAccountUser = useCaseCreateAccountUser;
            _useCaseGetUsersByAccountId = useCaseGetUsersByAccountId;
            _useCaseGetAccountsByUserId = useCaseGetAccountsByUserId;
            _useCaseDeleteAccountUser = useCaseDeleteAccountUser;
        }

        [HttpPost]
        [ProducesResponseType(201)]
        public ActionResult<OutputDtoAccountUser> Create(InputDtoAccountUser dto)
        {
            try
            {
                return StatusCode(201, _useCaseCreateAccountUser.Execute(dto));
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }

        [HttpGet]
        [ProducesResponseType(201)]
        [Route("IdAccount/{idAccount:int}")]
        public ActionResult<List<OutputDtoUser>> GetUsersByAccountId(int idAccount)
        {
            return StatusCode(201, _useCaseGetUsersByAccountId.Execute(idAccount));
        }

        [HttpGet]
        [ProducesResponseType(201)]
        [Route("IdUser/{idUser:int}")]
        public ActionResult<List<OutputDtoAccount>> GetAccountsByUserId(int idUser)
        {
            return StatusCode(201, _useCaseGetAccountsByUserId.Execute(idUser));
        }

        [HttpDelete]
        [ProducesResponseType(201)]
        [Route("Delete/{idUser:int}/{idAccount:int}")]
        public ActionResult<bool> DeleteAccountUser(int idUser, int idAccount)
        {
            return StatusCode(201, _useCaseDeleteAccountUser.Execute(idUser, idAccount));
        }
    }
}