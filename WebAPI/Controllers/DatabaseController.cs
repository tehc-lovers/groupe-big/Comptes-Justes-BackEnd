﻿using Infrastructure.SqlServer.System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;

namespace WebAPI.Controllers
{
    [ApiController]
    [Route("api/database")]
    public class DatabaseController: ControllerBase
    {
        public readonly IHostEnvironment Environment;
        public readonly IDatabaseManager DatabaseManager;

        public DatabaseController(IDatabaseManager databaseManager, IHostEnvironment environment)
        {
            DatabaseManager = databaseManager;
            Environment = environment;
        }

        [HttpGet]
        [Route("init")]
        public IActionResult CreateDatabaseAndTables()
        {
            if (Environment.IsProduction())
                return BadRequest("Only in dev");
            DatabaseManager.CreateDatabaseAndTables();
            return Ok("Database and tables created successfully");
        }

        [HttpGet]
        [Route("fill")]
        public IActionResult FillTables()
        {
            if (Environment.IsProduction())
                return BadRequest("Only in dev");
            DatabaseManager.FillTables();
            return Ok("Tables have been filled");
        }
    }
}