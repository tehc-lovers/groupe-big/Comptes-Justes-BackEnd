using Application.UseCases.AccountManager;
using Application.UseCases.Accounts;
using Application.UseCases.AccountUser;
using Application.UseCases.Bill;
using Application.UseCases.Transaction;
using Application.UseCases.TransactionAccount;
using Application.UseCases.TransactionUser;
using Application.UseCases.Users;
using Application.Validators;
using Infrastructure.SqlServer.Repositories.Account;
using Infrastructure.SqlServer.Repositories.AccountManager;
using Infrastructure.SqlServer.Repositories.AccountUser;
using Infrastructure.SqlServer.Repositories.Bill;
using Infrastructure.SqlServer.Repositories.Transaction;
using Infrastructure.SqlServer.Repositories.TransactionAccount;
using Infrastructure.SqlServer.Repositories.TransactionsUsers;
using Infrastructure.SqlServer.Repositories.User;
using Infrastructure.SqlServer.System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace WebAPI
{
    public class Startup
    {
        private static readonly string MyOrigins = "MyOrigins";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        
        public IConfiguration Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy(MyOrigins, builder =>
                {
                    /*builder.WithOrigins("http://localhost:4200")
                        .AllowAnyMethod()
                        .AllowAnyHeader();*/
                    builder.AllowAnyOrigin()
                        .AllowAnyMethod()
                        .AllowAnyHeader();
                });
            });

            //Add repos
            services.AddSingleton<IUserRepository, UserRepository>();
            services.AddSingleton<ITransactionRepository, TransactionRepository>();
            services.AddSingleton<IDatabaseManager, DatabaseManager>();
            services.AddSingleton<IAccountRepository, AccountRepository>();
            services.AddSingleton<IAccountManagerRepository, AccountManagerRepository>();
            services.AddSingleton<IAccountUserRepository, AccountUserRepository>();
            services.AddSingleton<ITransactionUserRepository, TransactionUserRepository>();
            services.AddSingleton<ITransactionAccountRepository, TransactionAccountRepository>();
            services.AddSingleton<IBillRepository, BillRepository>();

            services.AddSingleton<AccountUserFactory>();
            services.AddSingleton<AccountManagerFactory>();
            services.AddSingleton<AccountFactory>();
            services.AddSingleton<UserFactory>();
            services.AddSingleton<TransactionFactory>();
            services.AddSingleton<TransactionUserFactory>();
            services.AddSingleton<TransactionAccountFactory>();
            services.AddSingleton<BillFactory>();


            //Add UseCase
            services.AddSingleton<UseCaseCreateUser>();
            services.AddSingleton<UseCaseGetAllUser>();
            services.AddSingleton<UseCaseGetByIdUser>();
            services.AddSingleton<UseCaseUpDateUser>();
            
            services.AddSingleton<UseCaseGetAllAccounts>();
            services.AddSingleton<UseCaseCreateAccount>();
            services.AddSingleton<UseCaseGetAccountByName>();

            services.AddSingleton<UseCaseCreateTransaction>();
            services.AddSingleton<UseCaseGetAllTransaction>();
            services.AddSingleton<UseCaseGetByIdTransaction>();

            services.AddSingleton<UseCaseCreateAccountManager>();
            services.AddSingleton<UseCaseGetAccountByManager>();

            services.AddSingleton<UseCaseCreateAccountUser>();
            services.AddSingleton<UseCaseGetUsersByAccountId>();
            services.AddSingleton<UseCaseGetAccountsByUserId>();
            services.AddSingleton<UseCaseDeleteAccountUser>();

            services.AddSingleton<UseCaseCreateTransactionUser>();
            services.AddSingleton<UseCaseGetTransactionByUser>();
            services.AddSingleton<UseCaseGetAllTransactionsUser>();

            services.AddSingleton<UseCaseCreateTransactionAccount>();
            services.AddSingleton<UseCaseGetTransactionsByAccount>();

            services.AddSingleton<UseCaseCreateBill>();
            services.AddSingleton<UseCaseGetAllBill>();
            services.AddSingleton<UseCaseGetBillByIdTransaction>();

            //Add Validators
            services.AddSingleton<ValidatorUsers>();
            
            
            services.AddControllers();
            services.AddSwaggerGen(c => { c.SwaggerDoc("v1", new OpenApiInfo() {Title = "My API", Version = "v1"}); });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage(); 
                app.UseSwagger(); 
                app.UseSwaggerUI(c => { c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1"); });
            }
            else
            {
                app.UseHttpsRedirection();
            }

            app.UseCors(MyOrigins);

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}