﻿using System.Data.SqlClient;

namespace Infrastructure.SqlServer
{
    public class Database
    {
        /* WINDOWS */
        private const string ConnectionString =
            "Server=LAPTOP-9U0H5TN6;Database=JustesComptes;Integrated Security=SSPI";
        
        /* UNIX */
        /*private const string ConnectionString = "Server=127.0.0.1,1433;Database=JustesComptes;User Id=SA;Password=P455W0rD;";*/

        public static SqlConnection GetConnection()
        {
            return new SqlConnection(ConnectionString);
        }
    }
}
