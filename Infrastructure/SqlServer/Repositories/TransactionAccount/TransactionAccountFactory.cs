﻿using System.Data.SqlClient;
using Infrastructure.SqlServer.Utils;
using NotImplementedException = System.NotImplementedException;

namespace Infrastructure.SqlServer.Repositories.TransactionAccount
{
    public class TransactionAccountFactory:IDomainFactory<Domain.TransactionAccount>
    {
        public Domain.TransactionAccount CreateFromSqlReader(SqlDataReader reader)
        {
            return new Domain.TransactionAccount()
            {
                Id = reader.GetInt32(reader.GetOrdinal(TransactionAccountRepository.ColId)),
                IdAccount = reader.GetInt32(reader.GetOrdinal(TransactionAccountRepository.ColIdAccount)),
                IdTransaction = reader.GetInt32(reader.GetOrdinal(TransactionAccountRepository.ColIdTransaction))
            };
        }
    }
}