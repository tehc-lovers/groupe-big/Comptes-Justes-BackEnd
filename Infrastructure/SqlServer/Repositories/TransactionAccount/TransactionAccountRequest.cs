﻿namespace Infrastructure.SqlServer.Repositories.TransactionAccount
{
    public partial class TransactionAccountRepository
    {
        public const string TableName = "transactions_accounts";
        public const string TableNameTransaction = "transactions";

        public const string ColId = "id",
            ColIdAccount = "id_account",
            ColIdTransaction = "id_transaction";

        private static readonly string ReqCreate = $@"
            INSERT INTO {TableName} ({ColIdAccount}, {ColIdTransaction})
            OUTPUT INSERTED.{ColId} VALUES(@{ColIdAccount},@{ColIdTransaction})";

        private static readonly string ReqGetTransactionsByAccount = $@"
            SELECT * FROM {TableNameTransaction} AS t INNER JOIN {TableName} ON t.{ColId}={ColIdTransaction}
            WHERE {ColIdAccount}=@{ColIdAccount}";

        private static readonly string ReqGetAllByIds = $@"
            SELECT * FROM {TableName} WHERE {ColIdAccount}=@{ColIdAccount}
            AND {ColIdTransaction}=@{ColIdTransaction}";


    }
}