﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Infrastructure.SqlServer.Repositories.Transaction;
using NotImplementedException = System.NotImplementedException;

namespace Infrastructure.SqlServer.Repositories.TransactionAccount
{
    public partial class TransactionAccountRepository:ITransactionAccountRepository
    {
        private TransactionAccountFactory _factory;
        private TransactionFactory _transactionFactory;

        public TransactionAccountRepository(TransactionAccountFactory factory, TransactionFactory transactionFactory)
        {
            _factory = factory;
            _transactionFactory = transactionFactory;
        }

        public Domain.TransactionAccount Create(Domain.TransactionAccount transactionAccount)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand()
            {
                Connection = connection,
                CommandText = ReqCreate
            };

            command.Parameters.AddWithValue("@" + ColIdAccount, transactionAccount.IdAccount);
            command.Parameters.AddWithValue("@" + ColIdTransaction, transactionAccount.IdTransaction);
            transactionAccount.Id = (int) command.ExecuteScalar();
            return transactionAccount;
        }

        public List<Domain.Transaction> GetTransactionsByAccount(int idAccount)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand()
            {
                Connection = connection,
                CommandText = ReqGetTransactionsByAccount
            };
            command.Parameters.AddWithValue("@" + ColIdAccount, idAccount);

            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            List<Domain.Transaction> transactions = new List<Domain.Transaction>();

            while (reader.Read())
            {
                transactions.Add(_transactionFactory.CreateFromSqlReader(reader));
            }

            return transactions;
        }

        public List<Domain.TransactionAccount> GetAllByIds(int idAccount, int idTransaction)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand()
            {
                Connection = connection,
                CommandText = ReqGetAllByIds
            };

            command.Parameters.AddWithValue("@" + ColIdAccount, idAccount);
            command.Parameters.AddWithValue("@" + ColIdTransaction, idTransaction);

            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            List<Domain.TransactionAccount> list = new List<Domain.TransactionAccount>();

            while (reader.Read())
            {
                list.Add(_factory.CreateFromSqlReader(reader));
            }

            return list;
        }
    }
}