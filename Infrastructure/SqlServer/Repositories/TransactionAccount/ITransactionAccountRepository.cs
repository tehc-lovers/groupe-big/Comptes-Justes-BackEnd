﻿using System.Collections.Generic;

namespace Infrastructure.SqlServer.Repositories.TransactionAccount
{
    public interface ITransactionAccountRepository
    {
        Domain.TransactionAccount Create(Domain.TransactionAccount transactionAccount);
        List<Domain.Transaction> GetTransactionsByAccount(int idAccount);
        List<Domain.TransactionAccount> GetAllByIds(int idAccount, int idTransaction);
    }
}