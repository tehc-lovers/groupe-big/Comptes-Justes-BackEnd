﻿using System.Collections.Generic;
using Infrastructure.SqlServer.Utils;

namespace Infrastructure.SqlServer.Repositories.Bill
{
    public interface IBillRepository:IElementQueryableById<Domain.Bill>
    {
        Domain.Bill Create(Domain.Bill bill);
        List<Domain.Bill> GetBillsByPath(string path);
        List<Domain.Bill> GetAll();
        Domain.Bill GetBillByIdTransaction(int idTransaction);
    }
}