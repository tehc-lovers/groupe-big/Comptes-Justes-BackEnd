﻿using System.Data.SqlClient;
using Infrastructure.SqlServer.Utils;

namespace Infrastructure.SqlServer.Repositories.Bill
{
    public class BillFactory:IDomainFactory<Domain.Bill>
    {
        public Domain.Bill CreateFromSqlReader(SqlDataReader reader)
        {
            return new Domain.Bill()
            {
                Id = reader.GetInt32(reader.GetOrdinal(BillRepository.ColId)),
                IdTransaction = reader.GetInt32(reader.GetOrdinal(BillRepository.ColIdTransaction)),
                RelativePath = reader.GetString(reader.GetOrdinal(BillRepository.ColPath))
            };
        }
    }
}