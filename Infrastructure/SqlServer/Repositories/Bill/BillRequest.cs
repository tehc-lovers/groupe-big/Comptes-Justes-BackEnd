﻿namespace Infrastructure.SqlServer.Repositories.Bill
{
    public partial class BillRepository
    {
        public const string TableName = "bills";

        public const string ColId = "id",
            ColIdTransaction = "id_transaction",
            ColPath = "relative_path";

        public static readonly string ReqCreateBill = $@"
            INSERT INTO {TableName} ({ColPath}, {ColIdTransaction})
            OUTPUT INSERTED.{ColId} VALUES (@{ColPath}, @{ColIdTransaction})";

        public static readonly string ReqGetBillsByPath = $@"
            SELECT * FROM {TableName} WHERE {ColPath} LIKE @{ColPath}";

        private static readonly string ReqGetAll = $@"
            SELECT * FROM {TableName}";

        private static readonly string ReqGetById = $@"
            SELECT * FROM {TableName} WHERE {ColId}=@{ColId}";

        private static readonly string ReqGetBillByTransactionId = $@"
            SELECT * FROM {TableName} WHERE {ColIdTransaction}=@{ColIdTransaction}";
    }
}