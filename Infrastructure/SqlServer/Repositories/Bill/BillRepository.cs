﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.VisualBasic;
using NotImplementedException = System.NotImplementedException;

namespace Infrastructure.SqlServer.Repositories.Bill
{
    public partial class BillRepository:IBillRepository
    {

        private BillFactory _factory;

        public BillRepository(BillFactory factory)
        {
            _factory = factory;
        }

        public Domain.Bill GetById(int id)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand()
            {
                Connection = connection,
                CommandText = ReqGetById
            };
            command.Parameters.AddWithValue("@" + ColId, id);
            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            
            return _factory.CreateFromSqlReader(reader);
        }

        public Domain.Bill Create(Domain.Bill bill)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand()
            {
                Connection = connection,
                CommandText = ReqCreateBill
            };

            command.Parameters.AddWithValue("@" + ColPath, "/assets/Images/"+bill.RelativePath);
            
            command.Parameters.AddWithValue("@" + ColIdTransaction, bill.IdTransaction);
            bill.Id = (int) command.ExecuteScalar();

            return bill;
        }

        public List<Domain.Bill> GetBillsByPath(string path)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand()
            {
                Connection = connection,
                CommandText = ReqGetBillsByPath
            };

            command.Parameters.AddWithValue("@" + ColPath, "/assets/Images/"+path);
            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            List<Domain.Bill> bills = new List<Domain.Bill>();

            while (reader.Read())
            {
                bills.Add(_factory.CreateFromSqlReader(reader));
            }

            return bills;
        }

        public List<Domain.Bill> GetAll()
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand()
            {
                Connection = connection,
                CommandText = ReqGetAll
            };

            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            List<Domain.Bill> bills = new List<Domain.Bill>();
            while (reader.Read())
            {
                bills.Add(_factory.CreateFromSqlReader(reader));
            }
            return bills;
        }

        public Domain.Bill GetBillByIdTransaction(int idTransaction)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand()
            {
                Connection = connection,
                CommandText = ReqGetBillByTransactionId
            };

            command.Parameters.AddWithValue("@" + ColIdTransaction, idTransaction);
            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);

            List<Domain.Bill> bills = new List<Domain.Bill>();
            while (reader.Read())
            {
                bills.Add(_factory.CreateFromSqlReader(reader));
            }

            return bills[0];
        }
    }
}