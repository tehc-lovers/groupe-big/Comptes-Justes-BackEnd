﻿namespace Infrastructure.SqlServer.Repositories.AccountUser
{
    public partial class AccountUserRepository
    {
        public const string TableName = "users_accounts";
        public const string TableNameAccount = "accounts";
        public const string TableNameUser = "users";

        public const string ColId = "id",
            ColIdUser = "id_user",
            ColIdAccount = "id_account",
            ColAddedDate = "added_date";

        public static readonly string ReqCreate = $@"
            INSERT INTO {TableName}({ColIdUser},{ColIdAccount},
            {ColAddedDate}) OUTPUT INSERTED.{ColId}
            VALUES(@{ColIdUser},@{ColIdAccount},@{ColAddedDate})";

        public static readonly string ReqGetPresentByIdUserAndIdAccount = $@"
            SELECT * FROM {TableName} WHERE {ColIdAccount}=@{ColIdAccount} AND {ColIdUser}=@{ColIdUser}";

        public static readonly string ReqGetUsersByIdAccount = $@"
            SELECT * FROM {TableNameUser} u INNER JOIN {TableName} ON u.{ColId}={ColIdUser} WHERE {ColIdAccount}=@{ColIdAccount}";

        public static readonly string ReqGetAccountsByIdUser = $@"
            SELECT * FROM {TableNameAccount} a INNER JOIN {TableName} ON a.{ColId}={ColIdAccount} WHERE {ColIdUser}= @{ColIdUser}";

        public static readonly string ReqDeleteByIdAccountAndIdUser = $@"
            DELETE FROM {TableName} WHERE {ColIdAccount}=@{ColIdAccount} AND {ColIdUser}=@{ColIdUser}";
    }
}