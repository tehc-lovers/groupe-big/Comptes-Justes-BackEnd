﻿using System.Collections.Generic;

namespace Infrastructure.SqlServer.Repositories.AccountUser
{
    public interface IAccountUserRepository
    {
        Domain.AccountUser Create(Domain.AccountUser accountUser);
        List<Domain.AccountUser> GetAccountByAccountId(int dtoIdAccount, int dtoIdUser);
        List<Domain.User> GetUsersByAccountId(int idAccount);
        List<Domain.Account> GetAccountsByUserId(int idUser);
        bool DeleteByIdAccountAndIdUser(int idAccount, int idUser);
    }
}