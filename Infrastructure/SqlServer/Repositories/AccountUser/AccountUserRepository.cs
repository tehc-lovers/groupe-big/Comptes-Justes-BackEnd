﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Infrastructure.SqlServer.Repositories.Account;
using Infrastructure.SqlServer.Repositories.User;
using NotImplementedException = System.NotImplementedException;

namespace Infrastructure.SqlServer.Repositories.AccountUser
{
    public partial class AccountUserRepository:IAccountUserRepository
    {
        private AccountUserFactory _accountUserFactory;
        private UserFactory _userFactory;
        private AccountFactory _accountFactory;

        public AccountUserRepository(AccountUserFactory accountUserFactory, AccountFactory accountFactory, UserFactory userFactory)
        {
            _accountUserFactory = accountUserFactory;
            _accountFactory = accountFactory;
            _userFactory = userFactory;
        }

        public Domain.AccountUser Create(Domain.AccountUser accountUser)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand
            {
                Connection = connection,
                CommandText = ReqCreate
            };

            command.Parameters.AddWithValue("@" + ColIdUser, accountUser.IdUser);
            command.Parameters.AddWithValue("@" + ColIdAccount, accountUser.IdAccount);
            command.Parameters.AddWithValue("@" + ColAddedDate, DateTime.Now);
            accountUser.Id = (int) command.ExecuteScalar();

            return accountUser;
        }

        public List<Domain.AccountUser> GetAccountByAccountId(int dtoIdAccount, int dtoIdUser)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand
            {
                Connection = connection,
                CommandText = ReqGetPresentByIdUserAndIdAccount
            };

            command.Parameters.AddWithValue("@" + ColIdAccount, dtoIdAccount);
            command.Parameters.AddWithValue("@" + ColIdUser, dtoIdUser);

            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            List<Domain.AccountUser> accountUsers = new List<Domain.AccountUser>();
            while (reader.Read())
            {
                accountUsers.Add(_accountUserFactory.CreateFromSqlReader(reader));
            }

            return accountUsers.Count == 0 ? null : accountUsers;
        }

        public List<Domain.User> GetUsersByAccountId(int idAccount)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand
            {
                Connection = connection,
                CommandText = ReqGetUsersByIdAccount
            };

            command.Parameters.AddWithValue("@" + ColIdAccount, idAccount);

            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            List<Domain.User> users = new List<Domain.User>();

            while (reader.Read())
            {
                users.Add(_userFactory.CreateFromSqlReader(reader));
            }

            return users;

        }

        public List<Domain.Account> GetAccountsByUserId(int idUser)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand
            {
                Connection = connection,
                CommandText = ReqGetAccountsByIdUser
            };
            command.Parameters.AddWithValue("@" + ColIdUser, idUser);
            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            List<Domain.Account> comptes = new List<Domain.Account>();
            while (reader.Read())
            {
                comptes.Add(_accountFactory.CreateFromSqlReader(reader));
            }

            return comptes;
        }

        public bool DeleteByIdAccountAndIdUser(int idAccount, int idUser)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand
            {
                Connection = connection,
                CommandText = ReqDeleteByIdAccountAndIdUser
            };

            command.Parameters.AddWithValue("@" + ColIdAccount, idAccount);
            command.Parameters.AddWithValue("@" + ColIdUser, idUser);

            return command.ExecuteNonQuery() > 0;
        }
    }
}