﻿using System.Data.SqlClient;
using Infrastructure.SqlServer.Utils;

namespace Infrastructure.SqlServer.Repositories.AccountUser
{
    public class AccountUserFactory:IDomainFactory<Domain.AccountUser>
    {
        public Domain.AccountUser CreateFromSqlReader(SqlDataReader reader)
        {
            return new Domain.AccountUser
            {
                Id = reader.GetInt32(reader.GetOrdinal(AccountUserRepository.ColId)),
                IdAccount = reader.GetInt32(reader.GetOrdinal(AccountUserRepository.ColIdAccount)),
                IdUser = reader.GetInt32(reader.GetOrdinal(AccountUserRepository.ColIdUser)),
                AddedDate = reader.GetDateTime(reader.GetOrdinal(AccountUserRepository.ColAddedDate))
            };
        }
    }
}