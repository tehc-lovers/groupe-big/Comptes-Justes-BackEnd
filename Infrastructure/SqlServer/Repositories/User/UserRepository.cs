﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Infrastructure.SqlServer.Utils;
using NotImplementedException = System.NotImplementedException;

namespace Infrastructure.SqlServer.Repositories.User
{
    public partial class UserRepository:IUserRepository
    {
        private readonly IDomainFactory<Domain.User> _factory = new UserFactory();

        public List<Domain.User> GetAll()
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand
            {
                Connection = connection,
                CommandText = ReqGetAll
            };

            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            List<Domain.User> users = new List<Domain.User>();
            while (reader.Read())
            {
                users.Add(_factory.CreateFromSqlReader(reader));
            }

            return users;
        }

        public Domain.User Create(Domain.User user)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand
            {
                Connection = connection,
                CommandText = ReqCreate
            };

            command.Parameters.AddWithValue("@"+ColFirstName, user.FirstName);
            command.Parameters.AddWithValue("@"+ColLastName, user.LastName);
            command.Parameters.AddWithValue("@"+ColEmail, user.Email);
            command.Parameters.AddWithValue("@"+ColNickname, user.Nickname);
            command.Parameters.AddWithValue("@"+ColPassword, user.Password);
            command.Parameters.AddWithValue("@"+ColAccountNumber, user.AccountNumber);
            user.Id = (int) command.ExecuteScalar();
            return user;
        }

        public Domain.User GetById(int id)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand
            {
                Connection = connection,
                CommandText = ReqGetById
            };

            command.Parameters.AddWithValue("@" + ColId, id);
            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);

            return reader.Read() ? _factory.CreateFromSqlReader(reader) : null;
        }

        public bool Update(int id, Domain.User userModifer)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand
            {
                Connection = connection,
                CommandText = ReqUpdate
            };

            command.Parameters.AddWithValue("@" + ColId, id);
            command.Parameters.AddWithValue("@" + ColFirstName, userModifer.FirstName);
            command.Parameters.AddWithValue("@" + ColLastName, userModifer.LastName);
            command.Parameters.AddWithValue("@" + ColEmail, userModifer.Email);
            command.Parameters.AddWithValue("@" + ColNickname, userModifer.Nickname);
            command.Parameters.AddWithValue("@" + ColPassword, userModifer.Password);
            command.Parameters.AddWithValue("@" + ColAccountNumber, userModifer.AccountNumber);

            return command.ExecuteNonQuery() > 0;
        }

        
    }
}