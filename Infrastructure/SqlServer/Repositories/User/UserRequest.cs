﻿namespace Infrastructure.SqlServer.Repositories.User
{
    public partial class UserRepository
    {
        public const string TableName = "users";

        public const string ColId = "id",
            ColFirstName = "first_name",
            ColLastName = "last_name",
            ColEmail = "email",
            ColNickname = "nickname",
            ColPassword = "password",
            ColAccountNumber = "account_number";

        public static readonly string ReqCreate = $@"
            INSERT INTO {TableName}({ColFirstName},{ColLastName},{ColEmail},{ColNickname},
            {ColPassword},{ColAccountNumber}) OUTPUT INSERTED.{ColId}
            VALUES(@{ColFirstName},@{ColLastName},@{ColEmail}, 
            @{ColNickname}, @{ColPassword}, @{ColAccountNumber})";

        public static readonly string ReqGetAll = $@"
            SELECT * FROM {TableName}";

        public static readonly string ReqGetById = $@"
            SELECT * FROM {TableName} WHERE {ColId}=@{ColId}";

        public static readonly string ReqUpdate = $@"
            UPDATE {TableName} SET {ColFirstName}=@{ColFirstName}, {ColLastName}=@{ColLastName},
            {ColEmail}=@{ColEmail}, {ColNickname}=@{ColNickname}, {ColPassword}=@{ColPassword},
            {ColAccountNumber}=@{ColAccountNumber} WHERE {ColId}=@{ColId}";

    }
}