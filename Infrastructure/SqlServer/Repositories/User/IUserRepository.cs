﻿
using System.Collections.Generic;
using Infrastructure.SqlServer.Utils;

namespace Infrastructure.SqlServer.Repositories.User
{
    public interface IUserRepository: IElementQueryableById<Domain.User>
    {
        List<Domain.User> GetAll();
        Domain.User Create(Domain.User user);
        bool Update(int id, Domain.User userModifer);
    }
}