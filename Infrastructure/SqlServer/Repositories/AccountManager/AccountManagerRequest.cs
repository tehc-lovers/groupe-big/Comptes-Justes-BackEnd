﻿namespace Infrastructure.SqlServer.Repositories.AccountManager
{
    public partial class AccountManagerRepository
    {
        public const string TableName = "accounts_managers";
        public const string TableNameAccounts = "accounts";
        public const string ColId = "id";
        public const string ColIdManager = "id_manager", ColIdAccount = "id_account";

        public static readonly string ReqCreate = $@"
            INSERT INTO {TableName}({ColIdAccount}, {ColIdManager}) 
            OUTPUT INSERTED.{ColId} VALUES(@{ColIdAccount}, @{ColIdManager})";

        public static readonly string ReqGetAccountsByUser = $@"
            SELECT * FROM {TableNameAccounts} a INNER JOIN {TableName} ON a.{ColId}={ColIdAccount}
             WHERE {ColIdManager}=@{ColIdManager}";

        public static readonly string ReqGetAccountByAccountId = $@"
            SELECT * FROM {TableName} WHERE {ColIdAccount}= @{ColIdAccount}";
    }
}