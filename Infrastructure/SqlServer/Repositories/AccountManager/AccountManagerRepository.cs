﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Infrastructure.SqlServer.Repositories.Account;
using NotImplementedException = System.NotImplementedException;

namespace Infrastructure.SqlServer.Repositories.AccountManager
{
    public partial class AccountManagerRepository: IAccountManagerRepository
    {
        private AccountManagerFactory _factory;
        private AccountFactory _accountFactory;

        public AccountManagerRepository(AccountManagerFactory factory, AccountFactory accountFactory)
        {
            _factory = factory;
            _accountFactory = accountFactory;
        }

        public Domain.AccountManager Create(Domain.AccountManager accountManager)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand
            {
                Connection = connection,
                CommandText = ReqCreate
            };

            command.Parameters.AddWithValue("@" + ColIdAccount, accountManager.IdAccount);
            command.Parameters.AddWithValue("@" + ColIdManager, accountManager.IdManager);
            accountManager.Id = (int) command.ExecuteScalar();
            return accountManager;
        }

        public List<Domain.Account> GetByManagerId(int id)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand
            {
                Connection = connection,
                CommandText = ReqGetAccountsByUser
            };

            command.Parameters.AddWithValue("@" + ColIdManager, id);
            
            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            List<Domain.Account> accountManagers = new List<Domain.Account>();

            while (reader.Read())
            {
                accountManagers.Add(_accountFactory.CreateFromSqlReader(reader));
            }

            return accountManagers;
        }

        public List<Domain.AccountManager> GetAccountByAccountId(int id)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand
            {
                Connection = connection,
                CommandText = ReqGetAccountByAccountId
            };

            command.Parameters.AddWithValue("@" + ColIdAccount, id);

            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            List<Domain.AccountManager> comptes = new List<Domain.AccountManager>();
            while (reader.Read())
            { 
                comptes.Add(_factory.CreateFromSqlReader(reader));
            }

            return comptes.Count ==0 ? null: comptes;
        }
    }
}