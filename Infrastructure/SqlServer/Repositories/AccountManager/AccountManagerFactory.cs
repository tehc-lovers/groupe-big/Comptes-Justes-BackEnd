﻿using System.Data.SqlClient;
using Infrastructure.SqlServer.Utils;
using NotImplementedException = System.NotImplementedException;

namespace Infrastructure.SqlServer.Repositories.AccountManager
{
    public class AccountManagerFactory:IDomainFactory<Domain.AccountManager>
    {
        public Domain.AccountManager CreateFromSqlReader(SqlDataReader reader)
        {
            return new Domain.AccountManager()
            {
                Id = reader.GetInt32(reader.GetOrdinal(AccountManagerRepository.ColId)),
                IdAccount = reader.GetInt32(reader.GetOrdinal(AccountManagerRepository.ColIdAccount)),
                IdManager = reader.GetInt32(reader.GetOrdinal(AccountManagerRepository.ColIdManager))
            };
        }
    }
}