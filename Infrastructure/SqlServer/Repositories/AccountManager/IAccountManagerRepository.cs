﻿using System.Collections.Generic;

namespace Infrastructure.SqlServer.Repositories.AccountManager
{
    public interface IAccountManagerRepository
    {
        Domain.AccountManager Create(Domain.AccountManager accountManager);
        List<Domain.Account> GetByManagerId(int id);

        List<Domain.AccountManager> GetAccountByAccountId(int id);
    }
}