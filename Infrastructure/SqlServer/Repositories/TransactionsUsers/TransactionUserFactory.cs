﻿using System.Data.SqlClient;
using Domain;
using Infrastructure.SqlServer.Utils;
using NotImplementedException = System.NotImplementedException;

namespace Infrastructure.SqlServer.Repositories.TransactionsUsers
{
    public class TransactionUserFactory:IDomainFactory<Domain.TransactionUser>
    {
        public TransactionUser CreateFromSqlReader(SqlDataReader reader)
        {
            return new Domain.TransactionUser()
            {
                Id = reader.GetInt32(reader.GetOrdinal(TransactionUserRepository.ColId)),
                IdUser = reader.GetInt32(reader.GetOrdinal(TransactionUserRepository.ColIdUser)),
                IdTransaction = reader.GetInt32(reader.GetOrdinal(TransactionUserRepository.ColIdTransaction))
            };
        }
    }
}