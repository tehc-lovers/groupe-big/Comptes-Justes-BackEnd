﻿namespace Infrastructure.SqlServer.Repositories.TransactionsUsers
{
    public partial class TransactionUserRepository
    {
        public const string TableName = "transactions_users";
        public const string TableNameTransaction = "transactions";
        public const string TableNameUser = "users";

        public const string ColId = "id",
            ColIdUser = "id_user",
            ColIdTransaction = "id_transaction";

        private static readonly string ReqCreate = $@"
            INSERT INTO {TableName} ({ColIdUser}, {ColIdTransaction})
            OUTPUT INSERTED.{ColId} VALUES (@{ColIdUser},@{ColIdTransaction})";

        private static readonly string ReqGetTransactionByUser = $@"
            SELECT * FROM {TableNameTransaction} AS t INNER JOIN {TableName} ON t.{ColId}= {ColIdTransaction}
            WHERE {ColIdUser}=@{ColIdUser}";

        private static readonly string ReqGetAll = $@"
            SELECT * FROM {TableName}";

        private static readonly string ReqGetAllByIds = $@"
            SELECT * FROM {TableName} WHERE {ColIdTransaction}=@{ColIdTransaction} AND {ColIdUser}=@{ColIdUser}";
    }
}