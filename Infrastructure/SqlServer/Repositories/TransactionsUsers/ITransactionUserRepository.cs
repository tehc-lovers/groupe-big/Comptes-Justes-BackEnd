﻿using System.Collections.Generic;

namespace Infrastructure.SqlServer.Repositories.TransactionsUsers
{
    public interface ITransactionUserRepository
    {
        public Domain.TransactionUser Create(Domain.TransactionUser transactionUser);
        List<Domain.Transaction> GetAllTransactionByUser(int idUser);
        List<Domain.TransactionUser> GetAll();
        List<Domain.TransactionUser> GetAllByIds(int idUser, int idTransaction);
    }
}