﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Domain;
using Infrastructure.SqlServer.Repositories.Transaction;
using NotImplementedException = System.NotImplementedException;

namespace Infrastructure.SqlServer.Repositories.TransactionsUsers
{
    public partial class TransactionUserRepository:ITransactionUserRepository
    {
        private readonly TransactionUserFactory _factory;
        private readonly TransactionFactory _transactionFactory;

        public TransactionUserRepository(TransactionUserFactory factory, TransactionFactory transactionFactory)
        {
            _factory = factory;
            _transactionFactory = transactionFactory;
        }

        public TransactionUser Create(TransactionUser transactionUser)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand()
            {
                Connection = connection,
                CommandText = ReqCreate
            };

            command.Parameters.AddWithValue("@" + ColIdUser, transactionUser.IdUser);
            command.Parameters.AddWithValue("@" + ColIdTransaction, transactionUser.IdTransaction);
            transactionUser.Id = (int) command.ExecuteScalar();
            return transactionUser;
        }

        public List<Domain.Transaction> GetAllTransactionByUser(int idUser)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand()
            {
                Connection = connection,
                CommandText = ReqGetTransactionByUser
            };

            command.Parameters.AddWithValue("@" + ColIdUser, idUser);
            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            List<Domain.Transaction> transactions = new List<Domain.Transaction>();

            while (reader.Read())
            {
                transactions.Add(_transactionFactory.CreateFromSqlReader(reader));
            }

            return transactions;
        }

        public List<TransactionUser> GetAll()
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand()
            {
                Connection = connection,
                CommandText = ReqGetAll
            };

            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);

            List<TransactionUser> transactionUsers = new List<TransactionUser>();
            while (reader.Read())
            {
                transactionUsers.Add(_factory.CreateFromSqlReader(reader));
            }

            return transactionUsers;
        }

        public List<TransactionUser> GetAllByIds(int idUser, int idTransaction)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand()
            {
                Connection = connection,
                CommandText = ReqGetAllByIds
            };

            command.Parameters.AddWithValue("@" + ColIdTransaction, idTransaction);
            command.Parameters.AddWithValue("@" + ColIdUser, idUser);

            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            List<Domain.TransactionUser> list = new List<TransactionUser>();

            while (reader.Read())
            {
                list.Add(_factory.CreateFromSqlReader(reader));
            }

            return list;
        }
    }
}