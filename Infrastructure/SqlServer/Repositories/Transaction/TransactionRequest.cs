using System;

namespace Infrastructure.SqlServer.Repositories.Transaction
{
    public partial class TransactionRepository
    {
        public const string TableName = "transactions";

        public const string ColId = "id",
            ColAmount = "amount",
            ColNecessity = "necessity_level",
            ColDate = "date",
            ColCommunication = "communication";

        public static readonly string ReqCreate = $@"
            INSERT INTO {TableName}({ColAmount},{ColDate},{ColNecessity},{ColCommunication})
            OUTPUT INSERTED.{ColId} VALUES(@{ColAmount},@{ColDate},@{ColNecessity},@{ColCommunication})";

        public static readonly string ReqGetAll = $@"SELECT * FROM {TableName}";

        public static readonly string ReqGetById = $@"SELECT * FROM {TableName} WHERE {ColId}=@{ColId}";

        public static readonly string ReqGetIdByCommunication =
            $@"SELECT * FROM {TableName} WHERE {ColCommunication} LIKE @{ColCommunication}";
    }
}