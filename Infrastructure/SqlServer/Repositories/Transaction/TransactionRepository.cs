using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Infrastructure.SqlServer.Utils;
using NotImplementedException = System.NotImplementedException;

namespace Infrastructure.SqlServer.Repositories.Transaction
{
    public partial class TransactionRepository : ITransactionRepository
    {
        private readonly IDomainFactory<Domain.Transaction> _factory = new TransactionFactory();

        public List<Domain.Transaction> GetAll()
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand()
            {
                Connection = connection,
                CommandText = ReqGetAll
            };

            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            List<Domain.Transaction> transactions = new List<Domain.Transaction>();
            while (reader.Read())
            {
                transactions.Add(_factory.CreateFromSqlReader(reader));
            }

            return transactions;
        }

        public Domain.Transaction Create(Domain.Transaction transaction)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand
            {
                Connection = connection,
                CommandText = ReqCreate
            };

            command.Parameters.AddWithValue("@"+ColAmount, transaction.Amount);
            command.Parameters.AddWithValue("@"+ColDate, transaction.Date);
            command.Parameters.AddWithValue("@"+ColNecessity, transaction.Level);
            command.Parameters.AddWithValue("@"+ColCommunication, transaction.Communication);
            transaction.Id = (int) command.ExecuteScalar();
            return transaction;
        }


        public Domain.Transaction GetById(int id)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand()
            {
                Connection = connection,
                CommandText = ReqGetById
            };

            command.Parameters.AddWithValue("@" + ColId, id);
            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            return reader.Read() ? _factory.CreateFromSqlReader(reader) : null;
        }
    }
}