using System.Collections.Generic;
using Infrastructure.SqlServer.Utils;

namespace Infrastructure.SqlServer.Repositories.Transaction
{
    public interface ITransactionRepository:IElementQueryableById<Domain.Transaction>
    {
        List<Domain.Transaction> GetAll();
        Domain.Transaction Create(Domain.Transaction transaction);
    }
}