using System.Data.SqlClient;
using Infrastructure.SqlServer.Utils;

namespace Infrastructure.SqlServer.Repositories.Transaction
{
    public class TransactionFactory : IDomainFactory<Domain.Transaction>
    {
        public Domain.Transaction CreateFromSqlReader(SqlDataReader reader)
        {
            return new Domain.Transaction
            {
                Id = reader.GetInt32(reader.GetOrdinal(TransactionRepository.ColId)),
                Amount = reader.GetDecimal(reader.GetOrdinal(TransactionRepository.ColAmount)),
                Date = reader.GetString(reader.GetOrdinal(TransactionRepository.ColDate)),
                Level = reader.GetInt32(reader.GetOrdinal(TransactionRepository.ColNecessity)),
                Communication = reader.GetString(reader.GetOrdinal(TransactionRepository.ColCommunication))
            };
        }
        
    }
}