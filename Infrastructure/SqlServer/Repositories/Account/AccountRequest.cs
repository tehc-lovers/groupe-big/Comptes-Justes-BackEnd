﻿namespace Infrastructure.SqlServer.Repositories.Account
{
    public partial class AccountRepository
    {
        public const string TableName = "accounts";

        public const string ColId = "id",
            ColName = "name";

        public static readonly string ReqCreate = $@"
            INSERT INTO {TableName}({ColName}) 
            OUTPUT INSERTED.{ColId} VALUES(@{ColName})";

        public static readonly string ReqGetAll = $@"
            SELECT * FROM {TableName}";

        public static readonly string ReqGetById = $@"
            SELECT * FROM {TableName} WHERE {ColId}= @{ColId}";

        public static readonly string ReqGetByName = $@"SELECT * FROM {TableName} WHERE {ColName} LIKE @{ColName}";
    }
}