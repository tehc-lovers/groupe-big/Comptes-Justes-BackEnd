﻿using System.Collections.Generic;
using Infrastructure.SqlServer.Utils;

namespace Infrastructure.SqlServer.Repositories.Account
{
    public interface IAccountRepository:IElementQueryableById<Domain.Account>
    {
        List<Domain.Account> GetAll();
        Domain.Account Create(Domain.Account account);
        Domain.Account GetById(int id);
        List<Domain.Account> GetByName(string name);
        
    }
}