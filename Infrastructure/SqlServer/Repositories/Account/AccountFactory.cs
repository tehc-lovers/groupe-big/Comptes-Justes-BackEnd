﻿using System.Data.SqlClient;
using Infrastructure.SqlServer.Utils;
using NotImplementedException = System.NotImplementedException;

namespace Infrastructure.SqlServer.Repositories.Account
{
    public class AccountFactory:IDomainFactory<Domain.Account>
    {
        public Domain.Account CreateFromSqlReader(SqlDataReader reader)
        {
            return new Domain.Account
            {
                Id = reader.GetInt32(reader.GetOrdinal(AccountRepository.ColId)),
                Name = reader.GetString(reader.GetOrdinal(AccountRepository.ColName))
            };
        }
    }
}