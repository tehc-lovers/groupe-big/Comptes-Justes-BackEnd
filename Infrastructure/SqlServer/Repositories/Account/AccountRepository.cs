﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Infrastructure.SqlServer.Utils;
using NotImplementedException = System.NotImplementedException;

namespace Infrastructure.SqlServer.Repositories.Account
{
    public partial class AccountRepository: IAccountRepository
    {
        private readonly IDomainFactory<Domain.Account> _factory = new AccountFactory();
        public List<Domain.Account> GetAll()
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand
            {
                Connection = connection,
                CommandText = ReqGetAll
            };

            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            List<Domain.Account> comptes = new List<Domain.Account>();
            while (reader.Read())
            {
                comptes.Add(_factory.CreateFromSqlReader(reader));
            }

            return comptes;
        }

        public Domain.Account Create(Domain.Account account)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand
            {
                Connection = connection,
                CommandText = ReqCreate
            };

            command.Parameters.AddWithValue("@" + ColName, account.Name);
            account.Id = (int) command.ExecuteScalar();
            return account;
        }

        public Domain.Account GetById(int id)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand
            {
                Connection = connection,
                CommandText = ReqGetById
            };

            command.Parameters.AddWithValue("@" + ColId, id);
            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            return reader.Read() ? _factory.CreateFromSqlReader(reader) : null;
        }

        public List<Domain.Account> GetByName(string name)
        {
            using var connection = Database.GetConnection();
            connection.Open();

            var command = new SqlCommand
            {
                Connection = connection,
                CommandText = ReqGetByName
            };

            command.Parameters.AddWithValue("@" + ColName,"%"+ name+"%");

            var reader = command.ExecuteReader(CommandBehavior.CloseConnection);
            List<Domain.Account> comptes = new List<Domain.Account>();

            while (reader.Read())
            {
                comptes.Add(_factory.CreateFromSqlReader(reader));
            }

            return comptes;
        }
        
    }
}