IF EXISTS (SELECT * FROM sysobjects WHERE name='transactions_users' and xtype='U')
    DROP TABLE transactions_users;

IF EXISTS (SELECT * FROM sysobjects WHERE name='accounts_managers' and xtype='U')
    DROP TABLE accounts_managers;

IF EXISTS (SELECT * FROM sysobjects WHERE name='users_accounts' and xtype='U')
    DROP TABLE users_accounts;

IF EXISTS (SELECT * FROM sysobjects WHERE name='transactions_accounts' and xtype='U')
    DROP TABLE transactions_accounts;

IF EXISTS (SELECT * FROM sysobjects WHERE name='bills' and xtype='U')
    DROP TABLE bills;
IF EXISTS (SELECT * FROM sysobjects WHERE name='users' and xtype='U')
DROP TABLE users;

IF EXISTS (SELECT * FROM sysobjects WHERE name='accounts' and xtype='U')
DROP TABLE accounts;

IF EXISTS (SELECT * FROM sysobjects WHERE name='transactions' and xtype='U')
DROP TABLE transactions;



CREATE TABLE users(
    id INT IDENTITY PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    email VARCHAR(50) NOT NULL,
    nickname VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    account_number VARCHAR(34) NOT NULL,
);

CREATE TABLE accounts(
    id INT IDENTITY PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
);

CREATE TABLE accounts_managers(
    id INT IDENTITY PRIMARY KEY,
    id_manager INT FOREIGN KEY REFERENCES users(id),
    id_account INT FOREIGN KEY REFERENCES accounts(id)
);

CREATE TABLE users_accounts(
    id INT IDENTITY PRIMARY KEY,
    id_user INT FOREIGN KEY REFERENCES users(id),
    id_account INT FOREIGN KEY REFERENCES accounts(id),
    added_date datetime not null
);

CREATE TABLE transactions(
    id INT IDENTITY PRIMARY KEY,
    amount NUMERIC(10,2) NOT NULL,
    date VARCHAR(20) NOT NULL,
    necessity_level INT,
    communication VARCHAR(250)
);

CREATE TABLE transactions_users(
    id INT IDENTITY PRIMARY KEY,
    id_user INT FOREIGN KEY REFERENCES users(id),
    id_transaction INT FOREIGN KEY REFERENCES transactions(id)
);

CREATE TABLE transactions_accounts(
    id INT IDENTITY PRIMARY KEY,
    id_transaction INT FOREIGN KEY REFERENCES transactions(id),
    id_account INT FOREIGN KEY REFERENCES accounts(id)
);

CREATE TABLE bills(
    id INT IDENTITY PRIMARY KEY ,
    id_transaction INT NOT NULL FOREIGN KEY REFERENCES transactions(id),
    relative_path VARCHAR(1024) NOT NULL
)
