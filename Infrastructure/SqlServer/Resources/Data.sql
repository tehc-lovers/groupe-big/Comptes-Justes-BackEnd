SET IDENTITY_INSERT users OFF;

INSERT INTO users (first_name, last_name, email, nickname, password, account_number)
VALUES ('Guillaume', 'Lambert', 'la198116@student.helha.be', 'la198116','helhamons', 'BE198116'),
       ('Tanguy', 'Taminau', 'la199566@student.helha.be', 'la199566','helhamons', 'BE199566'),
       ('Giorgio', 'Caculli', 'la196672@student.helha.be', 'la196672','helhamons', 'BE196672'),
       ('Theo', 'Desmet', 'la188756@student.helha.be', 'la188756','helhamons', 'BE188756');


SET IDENTITY_INSERT accounts OFF;

INSERT INTO accounts(name) VALUES ('GuillaumeAccount'),
                                  ('TanguyAccount'),
                                  ('GiorgioAccount'),
                                  ('TheoAccount');

SET IDENTITY_INSERT accounts_managers OFF;

INSERT INTO accounts_managers(id_manager, id_account) VALUES (1, 1),
                                                             (2,2),
                                                             (3,3),
                                                             (4,4);

INSERT INTO users_accounts(id_user, id_account, added_date) VALUES (1, 1, GETDATE()),
                                                                   (2, 2, GETDATE()),
                                                                   (3, 3, GETDATE()),
                                                                   (4, 4, GETDATE());

SET IDENTITY_INSERT users_accounts OFF;

INSERT INTO users_accounts(id_user, id_account, added_date) VALUES (2,1, GETDATE()),
                                                                   (3,1, GETDATE()),
                                                                   (1,2, GETDATE()),
                                                                   (4,3, GETDATE());

SET IDENTITY_INSERT transactions OFF;

INSERT INTO transactions ( amount, date, necessity_level, communication) VALUES ( 0.2, '2021-12-24', 0, 'Première transaction'),
                                                                                ( 0.2, '2021-12-24', 0, 'Deuxième transaction'),
                                                                                ( 0.2, '2021-12-24', 0, 'Première transaction bis'),
                                                                                ( 0.2, '2021-12-24', 0, 'Première transaction tertiaire');

SET IDENTITY_INSERT transactions_users OFF;

INSERT INTO transactions_users ( id_transaction, id_user) VALUES ( 1, 1),
                                                                 ( 2, 2),
                                                                 ( 3, 4),
                                                                 ( 4, 4);

SET IDENTITY_INSERT transactions_accounts OFF;

INSERT INTO transactions_accounts( id_transaction, id_account) VALUES ( 1, 1),
                                                                      ( 2, 1),
                                                                      ( 3, 3),
                                                                      ( 4, 4);

SET IDENTITY_INSERT bills OFF;

INSERT INTO bills ( id_transaction, relative_path) VALUES ( 1, '/assets/Images/fou.jpeg'),
                                                          ( 2, '/assets/Images/fou2.jpeg'),
                                                          ( 2, '/assets/Images/fou3.jpeg'),
                                                          ( 4, '/assets/Images/ReceiptSwiss.jpg');
